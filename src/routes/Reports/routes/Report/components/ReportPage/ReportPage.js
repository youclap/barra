import React from 'react'
import PropTypes from 'prop-types'
import ReportItem from '../ReportItem'
import Reviews from 'components/Reviews/ReviewList'
import NewReviewCard from '../NewReviewCard'
import Grid from '@material-ui/core/Grid'
import classes from './ReportPage.scss'

export const ReportPage = ({ item, addReview }) => (
  <div>
    <h1 className={classes.title}>🏛👨‍⚖️ Clap Court 👩‍⚖️🏛 </h1>
    <Grid container spacing={32} className={classes.container}>
      <Grid>
        <div className={classes.reportItem}>
          <ReportItem item={item} />
          <Reviews item={item} />
        </div>
      </Grid>
      <Grid>
        <div className={classes.card}>
          <NewReviewCard onSubmit={addReview} />
        </div>
      </Grid>
    </Grid>
  </div>
)

ReportPage.propTypes = {
  item: PropTypes.object.isRequired,
  addReview: PropTypes.func
}

export default ReportPage
