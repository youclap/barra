import React from 'react'
import PropTypes from 'prop-types'
import Post from 'components/Post'
import UserItem from 'components/User/UserItem'
import ConfirmationDeleteDialog from 'components/ConfirmationDeleteDialog'
import Card from '@material-ui/core/Card'
import CardContent from '@material-ui/core/CardContent'
import Typography from '@material-ui/core/Typography'
import List from '@material-ui/core/List'
import ListItem from '@material-ui/core/ListItem'
import ListItemText from '@material-ui/core/ListItemText'
import ExpansionPanel from '@material-ui/core/ExpansionPanel'
import ExpansionPanelSummary from '@material-ui/core/ExpansionPanelSummary'
import ExpansionPanelDetails from '@material-ui/core/ExpansionPanelDetails'
import IconButton from '@material-ui/core/IconButton'
import Face from '@material-ui/icons/Face'
import ExpandMoreIcon from '@material-ui/icons/ExpandMore'
import DeleteIcon from '@material-ui/icons/Delete'
import RestoreIcon from '@material-ui/icons/Restore'
import classes from './Comment.scss'

const Comment = ({ comment, goToUser, newDialogOpen, toggleDialog, markAsDeleted, restoreComment }) => (
  <Card className={classes.card}>
    <ExpansionPanel>
      <ExpansionPanelSummary expandIcon={<ExpandMoreIcon />}>
        <Typography className={classes.heading}>Post Info</Typography>
        <Typography className={classes.secondaryHeading} />
      </ExpansionPanelSummary>
      <ExpansionPanelDetails>
        <Post id={comment.postID} />
      </ExpansionPanelDetails>
    </ExpansionPanel>
    <CardContent>
      <List>
        <ListItem>
          <Face />
          <UserItem id={comment.userID} goToUser={goToUser} />
          <ListItemText primary={comment.date} secondary="Date" />
          <IconButton className={classes.icon} onClick={toggleDialog}>
            {showDelete(comment, newDialogOpen, toggleDialog, markAsDeleted, restoreComment)}
          </IconButton>
        </ListItem>
        <ListItem>
          <div>
            <la>Comment:</la>
            <Typography component="p">{comment.value}</Typography>
          </div>
        </ListItem>
      </List>
    </CardContent>
  </Card>
)

export const showDelete = (comment, newDialogOpen, toggleDialog, markAsDeleted, restoreComment) => {
  if (comment.isDeleted) {
    return (
      <div>
        <RestoreIcon />
        <ConfirmationDeleteDialog
          dialogText={"By clicking 'OK' you will restore this comment. If you are not sure please fall back and think again."}
          open={newDialogOpen}
          onRequestClose={() => toggleDialog}
          onSubmit={() => restoreComment()} />
      </div>
    )
  } else {
    return (
      <div>
        <DeleteIcon />
        <ConfirmationDeleteDialog
          dialogText={"By clicking 'OK' you will mark as deleted this comment. If you are not sure please fall back and think again."}
          open={newDialogOpen}
          onRequestClose={() => toggleDialog}
          onSubmit={() => markAsDeleted()} />
      </div>
    )
  }
}

Comment.propTypes = {
  comment: PropTypes.object.isRequired,
  goToUser: PropTypes.func.IsRequired,
  newDialogOpen: PropTypes.bool.IsRequired,
  toggleDialog: PropTypes.func.IsRequired,
  markAsDeleted: PropTypes.func.IsRequired,
  restoreComment: PropTypes.func.IsRequired
}

export default Comment
