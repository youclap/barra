import { compose } from 'redux'
import { connect } from 'react-redux'
import { firestoreConnect } from 'react-redux-firebase'
import { withNotifications } from 'modules/notification'
import { spinnerWhileLoading } from 'utils/components'
import { withHandlers, withStateHandlers, pure } from 'recompose'
import { addDocumentToExplore, deleteDocumentFromExplore, getFeaturedArrayByType } from 'utils/explore'

export default compose(
  firestoreConnect([
    {
      collection: 'explore',
      orderBy: ['order']
    }
  ]),
  connect(({ firestore: { ordered: { explore } } }) => {
    if (!explore) {
      return {}
    }

    const featuredUsersIDs = getFeaturedArrayByType(explore, 'USER')
    const featuredChallengesIDs = getFeaturedArrayByType(explore, 'CHALLENGE')
    return {
      featuredUsers: featuredUsersIDs,
      featuredChallenges: featuredChallengesIDs
    }
  }),
  spinnerWhileLoading(['featuredUsers', 'featuredChallenges']),
  withNotifications,
  withStateHandlers(
    ({ initialDialogOpen = false }) => ({
      newDialogOpen: initialDialogOpen
    }),
    {
      toggleDialog: ({ newDialogOpen }) => () => ({
        newDialogOpen: !newDialogOpen
      })
    }
  ),
  withHandlers({
    updateExplore: ({ firestore, showError, showSuccess }) => items => {
      return updateDocuments(items, firestore)
        .then(() => showSuccess('Featured cards updated with success!'))
        .catch(error => {
          console.error('Error:', error)
          return showError(error.message || 'Error updating featured cards')
        })
    },
    deleteFeaturedCard: ({ firestore, showError, showSuccess }) => featuredCard => {
      return deleteDocumentFromExplore(firestore, featuredCard.id, featuredCard.type, featuredCard.order)
        .then(() => showSuccess('Explore Card deleted successfully'))
        .catch(error => {
          console.error('Error deleting featured card ', error)
          showError(error.message || 'Could not delete featured card ')
          return Promise.reject(error)
        })
    },
    addFeaturedCard: ({ firestore, showError, showSuccess, toggleDialog, featuredUsers, featuredChallenges }) => newFeaturedCard => {
      const value = newFeaturedCard.value
      const type = newFeaturedCard.type
      const order = type === 'USER' ? featuredUsers.length : featuredChallenges.length

      return addDocumentToExplore(firestore, value, type, order)
        .then(() => {
          toggleDialog()
          return showSuccess('Featured Card created with success!')
        })
        .catch(error => {
          toggleDialog()
          console.error('Error:', error)
          return showError(error.message || 'Error creating explore card ' + value)
        })
    }
  }),
  pure
)

const updateDocuments = (items, firestore) => {
  return Promise.all(
    items.map((item, index) => {
      return firestore
        .collection('explore')
        .doc(item.id)
        .update({
          order: index + 1
        })
    })
  )
}
