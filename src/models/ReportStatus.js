export const ReportStatus = {
  OPEN: 'open',
  REVIEWING: 'reviewing',
  CLOSED: 'closed'
}
