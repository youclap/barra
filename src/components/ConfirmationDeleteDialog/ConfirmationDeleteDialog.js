import React from 'react'
import PropTypes from 'prop-types'
import Button from '@material-ui/core/Button'
import Dialog from '@material-ui/core/Dialog'
import DialogActions from '@material-ui/core/DialogActions'
import DialogContent from '@material-ui/core/DialogContent'
import DialogContentText from '@material-ui/core/DialogContentText'
import DialogTitle from '@material-ui/core/DialogTitle'

const ConfirmationDeleteDialog = ({ dialogText, open, onRequestClose, onSubmit }) => (
  <Dialog
    open={open}
    onClose={onRequestClose}
    onSubmit={onSubmit}
    aria-labelledby="alert-dialog-title"
    aria-describedby="alert-dialog-description">
    <DialogTitle id="alert-dialog-title">
      {'hmm.. Are you sure❓ 🤔'}
    </DialogTitle>
    <DialogContent>
      <DialogContentText id="alert-dialog-description">
        {dialogText}
      </DialogContentText>
    </DialogContent>
    <DialogActions>
      <Button onClick={onRequestClose} color="primary">
        CANCEL
      </Button>
      <Button onClick={onSubmit} color="primary" autoFocus>
        OK
      </Button>
    </DialogActions>
  </Dialog>
)

ConfirmationDeleteDialog.PropTypes = {
  dialogText: PropTypes.string.isRequired,
  open: PropTypes.func.isRequired,
  onRequestClose: PropTypes.func.isRequired,
  onSubmit: PropTypes.func.isRequired
}

export default ConfirmationDeleteDialog
