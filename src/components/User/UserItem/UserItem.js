import React from 'react'
import PropTypes from 'prop-types'
import ListItemText from '@material-ui/core/ListItemText'

const UserItem = ({ user, goToUser }) => (
  <ListItemText
    primary={user.name}
    secondary={user.username}
    onClick={() => goToUser(user)} />
)

UserItem.propTypes = {
  user: PropTypes.object.isRequired,
  goToUser: PropTypes.func
}

export default UserItem
