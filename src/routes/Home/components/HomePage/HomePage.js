import React from 'react'
import classes from './HomePage.scss'
import ReportCardDashboard from '../ReportCardDashboard'
import ExploreCardDashboard from '../ExploreCardDashboard'
import ChallengesCardDashboard from '../ChallengesCardDashboard'

export const Home = () => (
  <div className={classes.container}>
    <ReportCardDashboard />
    <ExploreCardDashboard />
    <ChallengesCardDashboard />
  </div>
)

export default Home
