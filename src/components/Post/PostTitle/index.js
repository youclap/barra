import PostTitle from './PostTitle'
import enhance from '../Post.enhancer'

export default enhance(PostTitle)
