import moment from 'moment'

export class ReviewViewModel {
  constructor(review) {
    this.id = review.id
    this.date = review.createdAt
      ? moment.unix(review.createdAt.seconds).fromNow()
      : '🔨🔨🔨'
    this.information = review.information
    this.value = review.value
    this.userID = review.createdBy
  }
}
