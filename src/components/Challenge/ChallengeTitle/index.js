import ChallengeTitle from './ChallengeTitle'
import enhance from '../Challenge.enhancer'

export default enhance(ChallengeTitle)
