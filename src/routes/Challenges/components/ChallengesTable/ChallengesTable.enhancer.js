import { compose } from 'redux'
import { connect } from 'react-redux'
import { withHandlers, withStateHandlers, lifecycle } from 'recompose'
import { firestoreConnect } from 'react-redux-firebase'
import { withRouter, spinnerWhileLoading } from 'utils/components'
import { ChallengeTableRowViewModel } from 'models/viewModels/ChallengeTableRowViewModel'
import { compareStrings, diffMoments } from 'utils/compare'
import { CHALLENGE_PATH } from 'constants'

export default compose(
  firestoreConnect([
    {
      collection: 'challenge',
      orderBy: ['timestamp', 'desc'],
      limit: 20
    }
  ]),
  connect(({ firestore: { ordered } }) => {
    if (!ordered.challenge) {
      return {}
    }

    const tableHeadCells = [
      { id: 'id', disablePadding: false, label: 'Item ID' },
      {
        id: 'thumbnail',
        disablePadding: false,
        label: 'Thumbnail',
        classes: { width: '100px' }
      },
      { id: 'userID', disablePadding: false, label: 'Creator' },
      { id: 'title', disablePadding: false, label: 'Title' },
      { id: 'startDateMoment', disablePadding: false, label: 'Start Date' },
      { id: 'endDateMoment', disablePadding: false, label: 'End Date' },
      { id: 'status', disablePadding: false, label: 'Status' }
    ]

    const challengesViewModel = ordered.challenge.map(
      challenge => new ChallengeTableRowViewModel(challenge)
    )
    return { challenges: challengesViewModel, tableHeadCells: tableHeadCells }
  }),
  spinnerWhileLoading(['challenges']),
  withRouter,
  withStateHandlers(
    ({ initialOrder = 'asc' }) => ({
      order: initialOrder,
      isLoading: false
    }),
    {
      handleRequestSort: (state, { challenges }) => (event, property) => {
        return {
          order:
            state.orderBy === property && state.order === 'desc'
              ? 'asc'
              : 'desc',
          orderBy: property,
          sortedChallenges: sortArray(challenges, state.order, property)
        }
      }
    }
  ),
  withHandlers({
    goToChallenge: ({ router }) => id => {
      console.log('challenge go to challenge id', id)
      router.push({
        pathname: `${CHALLENGE_PATH}/${id}`,
        state: id
      })
    },

    loadMoreChallenges: ({ firestore, challenges }) => () => {
      return firestore.get({ collection: 'challenge', 'orderBy': ['timestamp', 'desc'], 'limit': (challenges.length + 10) })
    }
  })
)

const getSort = (order, orderBy) => {
  return order === 'asc'
    ? (a, b) => compare(a, b, orderBy)
    : (a, b) => -compare(a, b, orderBy)
}

const compare = (a, b, orderBy) => {
  if (orderBy === 'startDateMoment' || orderBy === 'endDateMoment') {
    return diffMoments(a, b, orderBy)
  } else {
    return compareStrings(a, b, orderBy)
  }
}

const sortArray = (array, order, orderBy) => {
  return array.sort(getSort(order, orderBy))
}
