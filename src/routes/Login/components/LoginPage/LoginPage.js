import React from 'react'
import PropTypes from 'prop-types'
import Paper from '@material-ui/core/Paper'
import LoginForm from '../LoginForm'
import classes from './LoginPage.scss'

export const LoginPage = ({ emailLogin, onSubmitFail }) => (
  <div className={classes.container}>
    <Paper className={classes.panel}>
      <LoginForm onSubmit={emailLogin} onSubmitFail={onSubmitFail} />
    </Paper>
  </div>
)

LoginPage.propTypes = {
  emailLogin: PropTypes.func, // from enhancer (withHandlers)
  onSubmitFail: PropTypes.func, // from enhancer (withHandlers)
  googleLogin: PropTypes.func // from enhancer (withHandlers)
}

export default LoginPage
