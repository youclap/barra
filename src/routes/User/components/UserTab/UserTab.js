import React from 'react'
import PropTypes from 'prop-types'
import Challenges from 'components/Challenge/ChallengeList'
import Posts from 'components/Post/PostList'
import Comments from 'components/Comment/CommentList'
import Card from '@material-ui/core/Card'
import AppBar from '@material-ui/core/AppBar'
import Tabs from '@material-ui/core/Tabs'
import Tab from '@material-ui/core/Tab'
import classes from './UserTab.scss'

const A = 'Challenges';
const B = 'Posts';
const C = 'Comments';

class UserTab extends React.Component {
  state = {
    value: A
  }

  handleChange = (event, value) => {
    this.setState({ value })
  }

  render() {
    const { value } = this.state

    return (
      <Card className={classes.root}>
        <AppBar position="static">
          <Tabs value={value} onChange={this.handleChange}>
            <Tab value={A} className={classes.tab} label="Challenges" />
            <Tab value={B} className={classes.tab} label="Posts" />
            <Tab value={C} className={classes.tab} label="Comments" />
          </Tabs>
        </AppBar>
        {value === A && <Challenges challenges={this.props.challenges} />}
        {value === B && <Posts posts={this.props.posts} />}
        {value === C && <Comments comments={this.props.comments} />}
      </Card>
    )
  }
}

UserTab.propTypes = {
  handleChange: PropTypes.func,
  challenges: PropTypes.array.isRequired,
  posts: PropTypes.array.isRequired,
  comments: PropTypes.array.isRequired
}

export default UserTab
