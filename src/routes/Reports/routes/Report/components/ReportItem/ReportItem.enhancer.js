import { compose } from 'redux'
import { connect } from 'react-redux'
import { firestoreConnect } from 'react-redux-firebase'
import { spinnerWhileLoading } from 'utils/components'
import { pure, withStateHandlers, withHandlers } from 'recompose'
import { withNotifications } from 'modules/notification'
import { UserReportViewModel } from 'models/viewModels/UserReportViewModel'
import { ReportStatus } from 'models/ReportStatus'
import { ReportValue } from 'models/ReportValue'
import { updateDocumentField } from 'utils/updateField'

export default compose(
  firestoreConnect(({ item: { id } }) => [
    {
      collection: 'report',
      where: ['itemID', '==', id]
    }
  ]),
  connect(({ firestore: { ordered } }, { item }) => {
    if (!ordered || !ordered.report || !item) {
      return {}
    }
    const reports = ordered.report
    const reportsList = reports.map(report => new UserReportViewModel(report))
    return { reports: reportsList }
  }),
  spinnerWhileLoading(['reports']),
  withNotifications,
  withStateHandlers(
    ({ initialDialogOpen = false }) => ({
      newDialogIgnoreOpen: initialDialogOpen,
      newDialogDeleteOpen: initialDialogOpen
    }),
    {
      toggleIgnoreDialog: ({ newDialogIgnoreOpen }) => () => ({
        newDialogIgnoreOpen: !newDialogIgnoreOpen
      }),
      toggleDeleteDialog: ({ newDialogDeleteOpen }) => () => ({
        newDialogDeleteOpen: !newDialogDeleteOpen
      }),
    }
  ),
  withHandlers({
    ignoreReportItem: ({ firestore, reports, toggleIgnoreDialog, showSuccess, showError }) => item => {
      const reportsToUpdate = filterReportsByReportValue(reports, ReportValue.DELETE)

      const moderateItemPromise = createItemModerationDocument(firestore, item, ReportValue.IGNORE)
      const unmarkItemAsDeleted = updateDocumentField(firestore, item.type.toLowerCase(), item, 'deleted', false)
      const updateReportsPromise = updateReportsStatusAndValue(firestore, reportsToUpdate, ReportStatus.CLOSED, ReportValue.IGNORE)

      return Promise.all([moderateItemPromise, unmarkItemAsDeleted, updateReportsPromise])
        .then(() => {
          toggleIgnoreDialog()
          return showSuccess('Item and reports have been ignored with success!')
        })
        .catch(error => {
          console.error(
            'failed creating item moderation doc and updating reports with error ',
            error
          )
          toggleIgnoreDialog()
          return showError(error.message || 'Failure on reports update!')
        })
    },
    deleteItem: ({ firestore, reports, toggleDeleteDialog, showSuccess, showError }) => item => {
      const reportsToUpdate = filterReportsByReportValue(reports, ReportValue.IGNORE)

      const createModerateItemPromise = createItemModerationDocument(firestore, item, ReportValue.DELETE)
      const markItemAsDeleted = updateDocumentField(firestore, item.type.toLowerCase(), item, 'deleted', true)
      const updateReportsStatusPromise = updateReportsStatusAndValue(firestore, reportsToUpdate, ReportStatus.CLOSED, ReportValue.DELETE)

      return Promise.all([createModerateItemPromise, markItemAsDeleted, updateReportsStatusPromise])
        .then(() => {
          toggleDeleteDialog()
          showSuccess('Reports have been closed with success!')
        })
        .catch(error => {
          console.error(
            'failed creating item moderation doc and updating reports with error ',
            error
          )
          toggleDeleteDialog()
          return showError(error.message || 'Failure on reports update!')
        })
    }
  }),
  pure
)

const createItemModerationDocument = (firestore, item, value) => {
  return firestore
    .collection('item-moderation')
    .doc(item.id)
    .set({
      itemID: item.id,
      itemType: item.type,
      value: value,
      timestamp: firestore.FieldValue.serverTimestamp()
    })
    .then(doc =>
      console.log('item moderation document created with success! ', doc)
    )
    .catch(error =>
      console.error(
        'failed to create item moderation docID ',
        item.id,
        ' with error ',
        error
      )
    )
}

const updateReportsStatusAndValue = (firestoreInstance, reports, status, value) => {
  return reports.map(report => {
    return updateReport(firestoreInstance, report, status, value)
  })
}

const updateReport = (firestore, report, status, value) => {
  return firestore
    .collection('report')
    .doc(report.reportID)
    .update({ status: status, value: value })
    .then(() => {
      console.log('updated report with success!')
      return true
    })
    .catch(error => {
      console.log('error updating report ', report.id, ' with error ', error)
      return error
    })
}

const filterReportsByReportValue = (reports, value) => {
  return reports.filter(object => object.value === value || !object.status)
}
