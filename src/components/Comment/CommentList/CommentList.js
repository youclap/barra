import React from 'react'
import PropTypes from 'prop-types'
import CommentItem from '../CommentItem'
import classes from './CommentList.scss'

const CommentList = ({ comments }) => (
  <div className={classes.container}>
    {listComments(comments)}
  </div>
)

const listComments = (comments) => {
  if (comments.length > 0) {
    return comments.map(id => <CommentItem key={'comment-' + id} id={id} />)
  } else {
    return <p>0 comments created 😞</p>
  }
}

CommentList.propTypes = {
  comments: PropTypes.array.isRequired
}

export default CommentList
