import ReportPage from './ReportPage'
import enhance from './ReportPage.enhancer'
import { UserIsAdmin } from 'utils/router'

export default UserIsAdmin(enhance(ReportPage))
