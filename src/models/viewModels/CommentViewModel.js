import moment from 'moment'

const dateFormat = 'DD MM YYYY hh:mm a'

export class CommentViewModel {
  constructor(comment) {
    this.id = comment.uid
    this.date = moment.unix(comment.timestamp.seconds).format(dateFormat)
    this.isDeleted = comment.deleted
    this.postID = comment.postID
    this.value = comment.value
    this.userID = comment.userID
  }
}
