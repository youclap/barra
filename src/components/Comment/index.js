import Comment from './Comment'
import enhance from './Comment.enhancer'

export default enhance(Comment)
