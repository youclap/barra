import React from 'react'
import PropTypes from 'prop-types'
import Review from '../Review'
import Card from '@material-ui/core/Card'
import Typography from '@material-ui/core/Typography'
import List from '@material-ui/core/List'
import ExpansionPanel from '@material-ui/core/ExpansionPanel'
import ExpansionPanelSummary from '@material-ui/core/ExpansionPanelSummary'
import ExpansionPanelDetails from '@material-ui/core/ExpansionPanelDetails'
import ExpandMoreIcon from '@material-ui/icons/ExpandMore'
import classes from './../Review.scss'

const ReviewList = ({ reviews }) => loadReviews(reviews)

const loadReviews = (reviews) => {
  if (reviews.length > 0) {
    return (
      <Card className={classes.card}>
        <ExpansionPanel>
          <ExpansionPanelSummary expandIcon={<ExpandMoreIcon />}>
            <Typography className={classes.heading}>Review</Typography>
            <Typography className={classes.secondaryHeading}>
              Review List ({reviews.length})
        </Typography>
          </ExpansionPanelSummary>
          <ExpansionPanelDetails>
            <List>
              {reviews.map((review, index) => (
                <Review key={'review-' + index} review={review} />
              ))}
            </List>
          </ExpansionPanelDetails>
        </ExpansionPanel>
      </Card>
    )
  } else {
    return null
  }
}

ReviewList.propTypes = {
  reviews: PropTypes.array.isRequired
}

export default ReviewList
