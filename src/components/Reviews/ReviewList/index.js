import ReviewList from './ReviewList'
import enhance from './ReviewList.enhancer'

export default enhance(ReviewList)
