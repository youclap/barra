import { USER_PATH } from 'constants'

export default store => ({
  path: USER_PATH + '/:id',
  getComponent(nextState, cb) {
    require.ensure(
      [],
      require => {
        const UserPage = require('./components/UserPage').default
        cb(null, UserPage)
      },
      'UserPage'
    )
  }
})
