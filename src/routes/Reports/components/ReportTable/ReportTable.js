import * as React from 'react'
import PropTypes from 'prop-types'
import Table from '@material-ui/core/Table'
import TableBody from '@material-ui/core/TableBody'
import TableCell from '@material-ui/core/TableCell'
import ReportTableHead from './ReportTableHead'
import TableRow from '@material-ui/core/TableRow'
import Paper from '@material-ui/core/Paper'
import classes from './ReportTable.scss'

export const ReportTable = ({ reports, goToReportID, tableHeadCells, order, orderBy, handleRequestSort }) => (
  <Paper className={classes.root}>
    <Table className={classes.table}>
      <ReportTableHead cells={tableHeadCells} order={order} orderBy={orderBy} onRequestSort={handleRequestSort} />
      <TableBody>
        {reports.map((report) => {
          return (
            <TableRow
              hover
              key={report.itemID}
              onClick={() => goToReportID(report.itemID)}
              tabIndex={-1}
            >
              <TableCell
                component="th"
                scope="row"
                align="left">
                {report.itemID}
              </TableCell>
              <TableCell align="left">{report.itemType}</TableCell>
              <TableCell align="left">{report.lastReportDate}</TableCell>
              <TableCell align="right">{report.reportsCount}</TableCell>
              <TableCell align="right">{report.openReportsCount}</TableCell>
              <TableCell align="right">{report.status}</TableCell>
            </TableRow>
          )
        })}
      </TableBody>
    </Table>
  </Paper>
)

ReportTable.propTypes = {
  reports: PropTypes.array.isRequired,
  goToReportID: PropTypes.func,
  tableHeadCells: PropTypes.array.isRequired,
  order: PropTypes.string,
  orderBy: PropTypes.string,
  handleRequestSort: PropTypes.func
}

export default ReportTable
