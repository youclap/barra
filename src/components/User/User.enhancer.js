import { compose } from 'redux'
import { connect } from 'react-redux'
import { firestoreConnect } from 'react-redux-firebase'
import { withHandlers, withStateHandlers, pure } from 'recompose'
import { withNotifications } from 'modules/notification'
import { spinnerWhileLoading, withRouter } from 'utils/components'
import { UserViewModel } from 'models/viewModels/UserViewModel'
import { updateDocumentField } from 'utils/updateField'
import { addDocumentToExplore, featuredUsersCount } from 'utils/explore'
import { USER_PATH } from 'constants'

export default compose(
  firestoreConnect(({ id }) => [
    {
      collection: 'user',
      doc: id
    }
  ]),
  connect(({ firestore: { data } }, { id, showAddToExploreButton }) => {
    if (!data.user || !data.user[id]) {
      return {}
    }
    const user = data.user[id]
    const userViewModel = new UserViewModel(user)
    return { user: userViewModel, showAddToExploreButton }
  }),
  spinnerWhileLoading(['user']),
  withRouter,
  withNotifications,
  withStateHandlers(
    ({ initialDialogOpen = false }) => ({
      newDialogOpen: initialDialogOpen,
      addToExploreDialogOpen: initialDialogOpen
    }),
    {
      toggleDialog: ({ newDialogOpen }) => () => ({
        newDialogOpen: !newDialogOpen
      }),
      toggleAddToExploreDialog: ({ addToExploreDialogOpen }) => () => ({
        addToExploreDialogOpen: !addToExploreDialogOpen
      })
    }
  ),
  withHandlers({
    goToUser: ({ user, router }) => () => {
      router.push({
        pathname: `${USER_PATH}/${user.id}`,
        state: { user }
      })
    },

    markUserAsDeleted: ({ firestore, user, showSuccess, showError }) => () => {
      return updateDocumentField(firestore, 'user', user, 'deleted', true)
        .then(() => showSuccess('User marked as deleted!'))
        .catch(error => {
          console.error(error.message)
          return showError('Failed marking user as deleted!')
        })
    },

    restoreUser: ({ firestore, user, showSuccess, showError }) => () => {
      return updateDocumentField(firestore, 'user', user, 'deleted', false)
        .then(() => showSuccess('User restored with success!'))
        .catch(error => {
          console.error(error.message)
          return showError('Failed restoring deleted user!')
        })
    },

    addToExplore: ({ firestore, user, toggleAddToExploreDialog, showSuccess, showError }) => featuredUser => {
      toggleAddToExploreDialog()
      const order = parseInt(featuredUser.order)
      if (order) {
        return addDocumentToExplore(firestore, user.id, 'USER', order)
          .then(() => showSuccess('Featured Card created with success!'))
          .catch(error => {
            console.error('Error:', error)
            return showError(error.message || 'Error creating explore card ' + user.id)
          })
      } else {
        return featuredUsersCount(firestore)
          .then(usersCount => {
            return addDocumentToExplore(firestore, user.id, 'USER', usersCount)
              .then(() => showSuccess('Featured Card created with success!'))
              .catch(error => {
                console.error('Error:', error)
                return showError(error.message || 'Error creating explore card ' + user.id)
              })
          })
      }
    }
  }),
  pure
)
