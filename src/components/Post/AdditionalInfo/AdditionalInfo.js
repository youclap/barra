import React from 'react'
import PropTypes from 'prop-types'
import Button from '@material-ui/core/Button'
import Dialog from '@material-ui/core/Dialog'
import DialogActions from '@material-ui/core/DialogActions'
import DialogContent from '@material-ui/core/DialogContent'
import DialogContentText from '@material-ui/core/DialogContentText'
import Challenge from 'components/Challenge'
import classes from './AdditionalInfo.scss'

const ChallengeDialog = ({ challenge, open, onRequestClose }) => (
  <div className={classes.container}>
    <Dialog
      open={open}
      onClose={onRequestClose}
      aria-labelledby="form-dialog-title"
    >
      <DialogContent>
        <DialogContentText>
          <Challenge id={challenge}/>
        </DialogContentText>
      </DialogContent>
      <DialogActions>
        <Button onClick={onRequestClose} color="primary">
          Close
        </Button>
      </DialogActions>
    </Dialog>
  </div>
)

ChallengeDialog.PropTypes = {
  challenge: PropTypes.object.isRequired,
  open: PropTypes.func.isRequired,
  onRequestClose: PropTypes.func.isRequired
}

export default ChallengeDialog
