import React from 'react'
import PropTypes from 'prop-types'
import Card from '@material-ui/core/Card'
import classes from './CardIcon.scss'

const CardIcon = ({ Icon, className }) => (
  <Card className={classes.card}>
  <div className={classes.icon}>
    <Icon className={className} />
    </div>
  </Card>
)

CardIcon.propTypes = {
  Icon: PropTypes.object.isRequired,
  className: PropTypes.object.isRequired
}

export default CardIcon
