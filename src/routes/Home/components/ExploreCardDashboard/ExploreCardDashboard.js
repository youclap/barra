import React from 'react'
import { Link } from 'react-router'
import { EXPLORE_PATH } from 'constants'
import CardIcon from '../CardIcon/CardIcon'
import Card from '@material-ui/core/Card'
import CardContent from '@material-ui/core/CardContent'
import Typography from '@material-ui/core/Typography'
import Explore from '@material-ui/icons/Explore'
import classes from './ExploreCardDashboard.scss'

const ExploreCardDashboard = () => (
  <Link to={EXPLORE_PATH}>
    <div className={classes.main}>
      <Card className={classes.card}>
        <CardIcon Icon={Explore} className={classes.cardIcon} />
        <CardContent>
          <Typography variant="headline" component="h2">
            Explore
        </Typography>
        </CardContent>
      </Card>
    </div>
  </Link>
)

export default ExploreCardDashboard
