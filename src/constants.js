export const HOME_PATH = '/'
export const LOGIN_PATH = '/login'
export const REPORT_PATH = '/report'
export const EXPLORE_PATH = '/explore'
export const USER_PATH = '/user'
export const POST_PATH = '/post'
export const CHALLENGE_PATH = '/challenge'
export const LOGIN_FORM_NAME = 'login'
export const NEW_FEATURED_CARD = 'newFeaturedCard'
export const CHANGE_CHALLENGE_INFO ='changeChallengeInfo'
export const NEW_REVIEW_FORM_NAME = 'newReview'
export const NEW_FEATURED_CARD_WITH_ORDER = 'newFeaturedCardWithOrder'

export const formNames = {
  login: LOGIN_FORM_NAME
}

export const paths = {
  login: LOGIN_PATH,
  report: REPORT_PATH,
  explore: EXPLORE_PATH,
  challenges: CHALLENGE_PATH
}

export default { ...paths, ...formNames }
