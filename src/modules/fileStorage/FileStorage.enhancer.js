import { connect } from 'react-redux'
import { compose, pure } from 'recompose'
import { firebaseConnect } from 'react-redux-firebase'
import * as actions from 'modules/fileStorage/actions'

const mapDispatchToProps = (dispatch, ownProps) => {
  return dispatch(actions.getFileURL(ownProps.path, ownProps.id))
}

export default compose(
  firebaseConnect(),
  connect(
    null,
    mapDispatchToProps
  ),
  connect((state, props) => {
    if (!state || !state.fileStorage || !state.fileStorage.byId[props.id]) {
      return {}
    }
    const fileStorageObject = state.fileStorage.byId[props.id]
    return {
      url: fileStorageObject.url,
      mediaType: props.mediaType,
      classes: props.className
    }
  }),
)
