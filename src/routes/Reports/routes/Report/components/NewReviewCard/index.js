import NewReviewCard from './NewReviewCard'
import enhance from './NewReviewCard.enhancer'

export default enhance(NewReviewCard)
