import React from 'react'
import PropTypes from 'prop-types'
import classes from './ExplorePage.scss'
import Grid from '@material-ui/core/Grid'
import Typography from '@material-ui/core/Typography'
import Card from '@material-ui/core/Card'
import CardContent from '@material-ui/core/CardContent'
import Button from '@material-ui/core/Button'
import { arrayMove } from 'react-sortable-hoc'
import NewFeaturedCardDialog from '../NewFeaturedCardDialog'
import NewFeaturedCardTile from '../NewFeaturedCardTile'
import SortableList from 'components/SortableGridLayout'

class ExplorePage extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      featuredUsersCards: props.featuredUsers,
      featuredChallengesCards: props.featuredChallenges
    }

    this.handleClick = this.handleClick.bind(this)

    this.onUserSortEnd = ({ oldIndex, newIndex }) => {
      this.setState({
        featuredUsersCards: arrayMove(
          this.state.featuredUsersCards,
          oldIndex,
          newIndex
        )
      })
    }
    this.onChallengeSortEnd = ({ oldIndex, newIndex }) => {
      this.setState({
        featuredChallengesCards: arrayMove(
          this.state.featuredChallengesCards,
          oldIndex,
          newIndex
        )
      })
    }
  }

  handleClick(items) {
    this.props.updateExplore(items)
  }

  /*
   * OPTIMIZE - I am updating the component states based on featuredUsers and featuredChallenges props length (maybe there's a better way)
   * @param prevProps 
   */
  componentDidUpdate(prevProps) {
    if (prevProps.featuredUsers.length !== this.props.featuredUsers.length) {
      console.log(
        'user props are different, lets update state ',
        this.props.featuredUsers
      )
      this.state.featuredUsersCards = this.props.featuredUsers
    } else if (
      prevProps.featuredChallenges.length !==
      this.props.featuredChallenges.length
    ) {
      this.state.featuredChallengesCards = this.props.featuredChallenges
    }
  }

  render() {
    const featuredUsersCards = this.state.featuredUsersCards
    const featuredChallengesCards = this.state.featuredChallengesCards
    return (
      <div>
        <Grid className={classes.container}>
          <Card className={classes.card}>
            <CardContent>
              <Grid container spacing={32}>
                <Grid item xs={16}>
                  <Typography color="textSecondary">Featured Users</Typography>
                </Grid>
              </Grid>
              <Grid container spacing={32} className={classes.gridContainer}>
                <SortableList
                  items={featuredUsersCards}
                  onSortEnd={this.onUserSortEnd}
                  distance={10}
                  onDelete={this.props.deleteFeaturedCard}
                  axis="xy"
                />
              </Grid>
              <Button
                color="primary"
                onClick={() => this.handleClick(featuredUsersCards)}
              >
                Apply
              </Button>
                  <Grid container spacing={32}>
                    <Grid item xs={16}>
                      <Typography color="textSecondary">
                        Featured Challenges
                      </Typography>
                    </Grid>
                  </Grid>
                  <Grid container spacing={32} className={classes.gridContainer}>
                    <SortableList
                      items={featuredChallengesCards}
                      onSortEnd={this.onChallengeSortEnd}
                      distance={20}
                      onDelete={this.props.deleteFeaturedCard}
                      goToUser={this.props.goToUser}
                      axis="xy"
                    />
                  </Grid>
                  <Button
                    color="primary"
                    onClick={() => this.handleClick(featuredChallengesCards)}
                  >
                    Apply
                  </Button>
                </CardContent>
            </Card>
        </Grid>
        <div className={classes.floatButton}>
        <NewFeaturedCardTile onClick={this.props.toggleDialog} />
          <NewFeaturedCardDialog
            onSubmit={this.props.addFeaturedCard}
            open={this.props.newDialogOpen}
            onRequestClose={this.props.toggleDialog}
          />
          </div>
      </div>
    )
  }
}

ExplorePage.propTypes = {
  featuredUsers: PropTypes.array.isRequired,
  featuredChallenges: PropTypes.array.isRequired,
  updateExplore: PropTypes.func.isRequired,
  addFeaturedCard: PropTypes.func,
  deleteFeaturedCard: PropTypes.func.isRequired,
  newDialogOpen: PropTypes.bool,
  toggleDialog: PropTypes.func
}

export default ExplorePage
