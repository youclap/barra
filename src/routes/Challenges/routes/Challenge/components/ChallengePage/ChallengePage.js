import React from 'react'
import Challenge from 'components/Challenge'
import classes from './ChallengePage.scss'

const ChallengePage = ({ id }) => (
  <div className={classes.center}>
    <Challenge id={id} showAddToExploreButton={true} />
  </div>
)

export default ChallengePage
