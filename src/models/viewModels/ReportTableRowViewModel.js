import { ReportStatus } from '../ReportStatus'

export class ReportTableRowViewModel {
  constructor(report) {
    this.itemID = report.itemID
    this.itemType = report.itemType
    this.reportsCount = report.reports.length
    this.lastReportTimestamp = report.lastReportTimestamp
    this.lastReportDate = report.lastReportTimestamp.fromNow()
    this.openReportsCount = filterReportsByReportStatus(
      report.reports,
      undefined
    ).length
    this.status = this.openReportsCount === 0 ? '✅' : '❌'
  }
}

const filterReportsByReportStatus = (reports, reportStatus) => {
  return reports.filter(report => {
    return report.status === reportStatus
  })
}
