import React from 'react'
import PropTypes from 'prop-types'
import Typography from '@material-ui/core/Typography'
import classes from './ChallengeTitle.scss'

const ChallengeTitle = ({ challenge, goToChallenge, className }) => (
    <Typography
        gutterBottom
        variant="headline"
        component="h2"
        className={className}
        onClick={() => goToChallenge(challenge)}>
        <div>{challenge.title}</div>
    </Typography>
)

ChallengeTitle.propTypes = {
    challenge: PropTypes.object.isRequired,
    goToChallenge: PropTypes.func
}

export default ChallengeTitle
