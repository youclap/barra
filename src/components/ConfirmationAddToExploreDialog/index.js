import ConfirmationAddToExploreDialog from './ConfirmationAddToExploreDialog'
import enhance from './ConfirmationAddToExploreDialog.enhancer'

export default enhance(ConfirmationAddToExploreDialog)
