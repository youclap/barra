import React from 'react'
import PropTypes from 'prop-types'
import Typography from '@material-ui/core/Typography'

const UserUsername = ({ user, goToUser }) => (
    <Typography onClick={() => goToUser(user)}>
        {user.username}
    </Typography>
)

UserUsername.propTypes = {
    user: PropTypes.object.isRequired,
    goToUser: PropTypes.func.isRequired
}

export default UserUsername
