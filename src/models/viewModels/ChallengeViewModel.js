import moment from 'moment'
import { getHTMLElement } from 'utils/media'

const dateFormat = 'DD/MM/YYYY HH:mm'

export class ChallengeViewModel {
  constructor(challenge) {
    const startDateMoment = moment.unix(challenge.startDate.seconds)
    const endDateMoment = moment.unix(challenge.endDate.seconds)

    this.id = challenge.uid
    this.title = challenge.title
    this.description = challenge.description
    this.mediaType = getHTMLElement(challenge.media.type)
    this.path = 'challenge/' + challenge.uid + '/media'
    this.startDateMoment = startDateMoment
    this.endDateMoment = endDateMoment
    this.startDate = startDateMoment.format(dateFormat)
    this.endDate = endDateMoment.format(dateFormat)
    this.isDeleted = challenge.deleted
    this.userID = challenge.userID
  }
}
