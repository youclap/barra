export class UserViewModel {
  constructor(user) {
    const photoURL = user.photoURL

    this.id = user.uid
    this.username = user.username
    this.name = user.displayName
    this.email = user.email
    this.isDeleted = user.deleted
    this.photoURL = photoURL
      ? (photoURL.startsWith('http') ? photoURL : undefined)
      : undefined
    this.photoPath = photoURL
      ? (photoURL.startsWith('http') ? undefined : `user/${user.uid}/${photoURL}`)
      : undefined
  }
}
