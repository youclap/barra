import { CHALLENGE_PATH } from 'constants'

export default store => ({
  path: `${CHALLENGE_PATH}/:id`,
  getComponent(nextState, cb) {
    require.ensure(
      [],
      require => {
        const ChallengePage = require('./components/ChallengePage').default

        cb(null, ChallengePage)
      },
      'Challenge'
    )
  }
})
