import { compose } from 'redux'
import { connect } from 'react-redux'
import { firestoreConnect } from 'react-redux-firebase'
import { spinnerWhileLoading } from 'utils/components'

export default compose(
    firestoreConnect(({ params: { id } }) => [{
        collection: 'challenge',
        where: ['userID', '==', id]
    }]),
    connect(({ firestore: { ordered } }, { params: { id } }) => {
        if (!ordered || !ordered.challenge) {
            return {}
        }
        const userChallenges = filterArrayByUserID(ordered.challenge, id)
        const challengeIDs = getListOfIDs(userChallenges)

        console.log('challengeIDs ', challengeIDs)
        return { challenges: challengeIDs }
    }),
    spinnerWhileLoading(['challenges']),
    
    firestoreConnect(({ params: { id } }) => [{
        collection: 'post',
        where: ['userID', '==', id]
    }]),
    connect(({ firestore: { ordered } }, { params: { id } }) => {
        if (!ordered || !ordered.post) {
            return {}
        }

        const userPosts = filterArrayByUserID(ordered.post, id)
        const postIDs = getListOfIDs(userPosts)
        console.log('postIDs ', postIDs)
        return { posts: postIDs }
    }),
    spinnerWhileLoading(['posts']),

    firestoreConnect(({ params: { id } }) => [{
        collection: 'comment',
        where: ['userID', '==', id]
    }]),
    connect(({ firestore: { ordered } }) => {
        if (!ordered || !ordered.comment) {
            return {}
        }

        const commentIDs = getListOfIDs(ordered.comment)
        console.log('commentIDs ', commentIDs)
        return { comments: commentIDs }
    }),
    spinnerWhileLoading(['comments'])
)

const getListOfIDs = (array) => array.map(doc => doc.id)

const filterArrayByUserID = (array, value) => {
    return array.filter(object => object.userID === value)
}
