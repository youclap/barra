import ChallengesPage from './ChallengesPage'
import { UserIsAdmin } from 'utils/router'

export default UserIsAdmin(ChallengesPage)
