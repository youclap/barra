import User from './User'
import enhance from './User.enhancer'

export default enhance(User)
