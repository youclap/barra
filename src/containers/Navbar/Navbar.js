import React from 'react'
import PropTypes from 'prop-types'
import { Link } from 'react-router'
import AppBar from '@material-ui/core/AppBar'
import Toolbar from '@material-ui/core/Toolbar'
import Typography from '@material-ui/core/Typography'
import AccountMenu from './AccountMenu'
import LoginMenu from './LoginMenu'
import { LIST_PATH } from 'constants'
import classes from './Navbar.scss'
import Drawer from 'containers/Drawer'

export const Navbar = ({
  authExists,
  isAdmin,
  handleLogout,
  closeAccountMenu,
  anchorEl,
  handleMenu
}) => (
    <AppBar position="static">
      <Toolbar>
        <div className={classes.menuButton}>
        {isAdmin ? <Drawer /> : null}
        </div>
        <Typography
          type="title"
          color="inherit"
          className={classes.flex}
          Component={Link}
          to={isAdmin ? LIST_PATH : '/'}
        >
          Barra
      </Typography>
        {authExists ? (
          <AccountMenu
            onLogoutClick={handleLogout}
            closeAccountMenu={closeAccountMenu}
            handleMenu={handleMenu}
            anchorEl={anchorEl}
          />
        ) : (
            <LoginMenu />
          )}
      </Toolbar>
    </AppBar>
  )

Navbar.propTypes = {
  authExists: PropTypes.bool, // from enhancer (withProps - auth)
  isAdmin: PropTypes.bool,
  handleLogout: PropTypes.func.isRequired, // from enhancer (withHandlers - firebase)
  closeAccountMenu: PropTypes.func.isRequired, // from enhancer (withHandlers - firebase)
  handleMenu: PropTypes.func.isRequired, // from enhancer (withHandlers - firebase)
  anchorEl: PropTypes.object // from enhancer (withStateHandlers - handleMenu)
}

export default Navbar
