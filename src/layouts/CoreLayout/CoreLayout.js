import React from 'react'
import PropTypes from 'prop-types'
import Navbar from 'containers/Navbar'
import classes from './CoreLayout.scss'
import { Notifications } from 'modules/notification'
import Paper from '@material-ui/core/Paper'
import MenuList from 'containers/Drawer/MenuList'

export const CoreLayout = ({ children, authIsAdmin }) => (
  <div className={classes.container}>
    <div className={classes.navBar}>
      <Navbar />
    </div>
    <div className={classes.layout}>
      {showMenuList(authIsAdmin)}
      <main className={classes.children}>{children}</main>
      <Notifications />
    </div>
  </div>
)
const showMenuList = isAuthenticated => {
  if (isAuthenticated) {
    return (
      <Paper className={classes.paper}>
        <MenuList />
      </Paper>
    )
  }
}

CoreLayout.propTypes = {
  children: PropTypes.element.isRequired,
  authIsAdmin: PropTypes.bool
}

export default CoreLayout
