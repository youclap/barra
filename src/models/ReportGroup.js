import moment from 'moment'

export class ReportGroup {
  constructor(reportID, itemID, itemType, reports) {
    this.reportID = reportID
    this.itemID = itemID
    this.itemType = itemType
    this.reports = reports
    this.lastReportTimestamp = getLastReportTimestamp(reports)
  }
}

const getLastReportTimestamp = (reports) => {
  const momentArray = reports.map(report => moment.unix(report.timestamp))
  return moment.max(momentArray)
}
