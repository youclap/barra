export const compareNumbers = (a, b, orderBy) => {
  return a[orderBy] - b[orderBy]
}

export const diffMoments = (a, b, orderBy) => {
  return a[orderBy].diff(b[orderBy])
}

export const compareStrings = (a, b, orderBy) => {
  return a[orderBy].localeCompare(b[orderBy])
}
