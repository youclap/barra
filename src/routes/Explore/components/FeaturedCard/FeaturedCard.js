import React from 'react'
import PropTypes from 'prop-types'
import FeaturedUser from '../FeaturedUser'
import FeaturedChallenge from '../FeaturedChallenge'
import classes from './FeaturedCard.scss'

const FeaturedCard = ({ item, deleteFeaturedCard }) => (
  <div className={classes.divCard}>
    {loadFeaturedComponent(item, deleteFeaturedCard)}
  </div>
)

const loadFeaturedComponent = (item, deleteFeaturedCard) => {
  if (item.type === 'CHALLENGE') {
    return (
      <FeaturedChallenge
        item={item}
        id={item.value}
        deleteFeaturedCard={deleteFeaturedCard}
      />
    )
  } else if (item.type === 'USER') {
    return (
      <FeaturedUser
        item={item}
        id={item.value}
        deleteFeaturedCard={deleteFeaturedCard}
      />
    )
  }
}

FeaturedCard.propTypes = {
  item: PropTypes.object.isRequired,
  deleteFeaturedCard: PropTypes.func.isRequired
}

export default FeaturedCard
