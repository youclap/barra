import PostItem from './PostItem'
import enhance from '../Post.enhancer'
import enhancerItem from './PostItem.enhancer'

export default enhance(enhancerItem(PostItem))
