import Review from './Review'
import enhance from './Review.enhancer'

export default enhance(Review)
