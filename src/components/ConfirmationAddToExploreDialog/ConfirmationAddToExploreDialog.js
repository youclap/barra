import React from 'react'
import PropTypes from 'prop-types'
import Button from '@material-ui/core/Button'
import IconButton from '@material-ui/core/IconButton'
import Dialog from '@material-ui/core/Dialog'
import DialogActions from '@material-ui/core/DialogActions'
import DialogContent from '@material-ui/core/DialogContent'
import DialogContentText from '@material-ui/core/DialogContentText'
import DialogTitle from '@material-ui/core/DialogTitle'
import Tooltip from '@material-ui/core/Tooltip'
import AddIcon from '@material-ui/icons/Add'
import { Field } from 'redux-form'
import { TextField } from 'redux-form-material-ui'
import { required } from 'utils/form'
import classes from './ConfirmationAddToExploreDialog.scss'

const ConfirmationAddToExploreDialog = ({ dialogText, newDialogOpen, toggleDialog, handleSubmit }) => {
  
  return (
    <div>
      <IconButton onClick={toggleDialog}>
        <Tooltip title="Add to Explore" placement="bottom">
          <AddIcon className={classes.color} />
        </Tooltip>
      </IconButton>
      <Dialog
        open={newDialogOpen}
        onClose={toggleDialog}
        aria-labelledby="alert-dialog-title"
        aria-describedby="alert-dialog-description">
        <DialogTitle id="alert-dialog-title">
          {'hmm.. Are you sure❓ 🤔'}
        </DialogTitle>
        <form onSubmit={handleSubmit}>
          <DialogContent>
            <DialogContentText id="alert-dialog-description">
              {dialogText}
            </DialogContentText>
            <Field
              name="order"
              component={TextField}
              label="Explore card order"
              validate={[required]}
            />
          </DialogContent>
          <DialogActions>
            <Button onClick={toggleDialog} color="primary">
              CANCEL
      </Button>
            <Button onClick={handleSubmit} color="primary" autoFocus>
              OK
      </Button>
          </DialogActions>
        </form>
      </Dialog>
    </div>
  )
}

ConfirmationAddToExploreDialog.PropTypes = {
  dialogText: PropTypes.string.isRequired,
  onSubmit: PropTypes.func.isRequired
}

export default ConfirmationAddToExploreDialog
