import React from 'react'
import PropTypes from 'prop-types'
import Button from '@material-ui/core/Button'
import Dialog from '@material-ui/core/Dialog'
import DialogTitle from '@material-ui/core/DialogTitle'
import DialogActions from '@material-ui/core/DialogActions'
import DialogContent from '@material-ui/core/DialogContent'
import { Field } from 'redux-form'
import { TextField } from 'redux-form-material-ui'
import { required } from 'utils/form'
import Select from 'components/Select'
import classes from './NewFeaturedCardDialog.scss'

// maybe i should move this object to another place, it will be used by the MySelect component
const options = [
  {
    value: '',
    text: 'None'
  },
  {
    value: 'USER',
    text: 'User'
  },
  {
    value: 'CHALLENGE',
    text: 'Challenge'
  }
]

const NewFeaturedCardDialog = ({ open, onRequestClose, handleSubmit }) => (
  <Dialog open={open} onClose={onRequestClose}>
    <DialogTitle id="simple-dialog-title">New Featured Card</DialogTitle>
    <form onSubmit={handleSubmit} className={classes.inputs}>
      <DialogContent>
        <Field
          name="value"
          component={TextField}
          label="Featured Card ID"
          validate={[required]}
        />
        <p />
        <Field
          name="type"
          component={props => (
            <Select
              title="Featured card type"
              options={options}
              onChange={props.input.onChange}
              value={props.input.value}
            />
          )}
        />
      </DialogContent>
      <DialogActions>
        <Button onClick={onRequestClose} color="secondary">
          Cancel
        </Button>
        <Button onClick={handleSubmit} color="primary">
          Add
        </Button>
      </DialogActions>
    </form>
  </Dialog>
)

NewFeaturedCardDialog.propTypes = {
  open: PropTypes.bool,
  onRequestClose: PropTypes.func,
  handleSubmit: PropTypes.func
}

export default NewFeaturedCardDialog
