import React from 'react'
import PropTypes from 'prop-types'
import PostItem from './../PostItem'
import classes from './PostList.scss'

const PostList = ({ posts }) => (
  <div className={classes.container}>
    {listPosts(posts)}
  </div>
)

const listPosts = (posts) => {
  if (posts.length > 0) {
    return posts.map(id => <PostItem key={'post-' + id} id={id} />)
  } else {
    return <p>0 posts created 😞</p>
  }
}

PostList.PropTypes = {
  posts: PropTypes.array.isRequired
}

export default PostList
