import UserUsername from './UserUsername'
import enhance from '../User.enhancer'

export default enhance(UserUsername)
