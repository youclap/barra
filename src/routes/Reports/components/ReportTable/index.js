import ReportTable from './ReportTable'
import enhance from './ReportTable.enhancer'
import { UserIsAdmin } from 'utils/router'

export default UserIsAdmin(enhance(ReportTable))
