import React from 'react'
import PropTypes from 'prop-types'
import FileStorage from 'modules/fileStorage'
import ConfirmationDeleteDialog from 'components/ConfirmationDeleteDialog'
import ConfirmationAddToExploreDialog from 'components/ConfirmationAddToExploreDialog'
import Card from '@material-ui/core/Card'
import CardContent from '@material-ui/core/CardContent'
import CardMedia from '@material-ui/core/CardMedia'
import List from '@material-ui/core/List'
import ListItem from '@material-ui/core/ListItem'
import ListItemIcon from '@material-ui/core/ListItemIcon'
import ListItemText from '@material-ui/core/ListItemText'
import Grid from '@material-ui/core/Grid'
import Typography from '@material-ui/core/Typography'
import IconButton from '@material-ui/core/IconButton'
import Tooltip from '@material-ui/core/Tooltip'
import LaunchIcon from '@material-ui/icons/Launch'
import DeleteIcon from '@material-ui/icons/Delete'
import RestoreIcon from '@material-ui/icons/Restore'
import Face from '@material-ui/icons/Face'
import Person from '@material-ui/icons/Person'
import classes from './User.scss'

const User = ({
  user,
  toggleDialog,
  toggleAddToExploreDialog,
  newDialogOpen,
  addToExploreDialogOpen,
  markUserAsDeleted,
  restoreUser,
  showAddToExploreButton,
  addToExplore,
  goToUser
}) => (
    <Card className={classes.card}>
      {loadUserImage(user)}
      <CardContent>
        <Grid container spacing={16} className={classes.grid}>
          <Grid className={classes.titleGrid}>
            <Typography gutterBottom variant="headline" component="h2">
              {user.email}
            </Typography>
          </Grid>
          <Grid className={classes.iconGrid}>
            <IconButton onClick={toggleDialog}>
              {showDelete(user, markUserAsDeleted, restoreUser, newDialogOpen, toggleDialog)}
            </IconButton>
            {showButtons(
              showAddToExploreButton,
              goToUser,
              addToExplore,
              addToExploreDialogOpen,
              toggleAddToExploreDialog
            )}
          </Grid>
        </Grid>
        <Grid className={classes.iconGrid}>
          <IconButton className={classes.icon} onClick={toggleDialog}>
            {showDelete(user, markUserAsDeleted, restoreUser, newDialogOpen, toggleDialog)}
          </IconButton>
        </Grid>
      
      <List>
        <div className={classes.listItemPosition}>
          <ListItem>
            <Face />
            <ListItemText primary={user.username} secondary="Username" />
          </ListItem>
          <ListItem>
            <Person />
            <ListItemText primary={user.name} secondary="Name" />
          </ListItem>
        </div>
      </List>
    </CardContent>
  </Card >
)

const loadUserImage = user => {
  if (user.photoURL) {
    return (
      <CardMedia
        className={classes.media}
        image={user.photoURL}
        title={user.username} />
    )
  } else if (user.photoPath) {
    return <FileStorage path={user.photoPath} mediaType={'img'} id={user.id} />
  } else {
    return <p>No photoURL for this user</p>
  }
}

const showDelete = (user, markUserAsDeleted, restoreUser, openDialog, toggleDialog) => {
  if (user.isDeleted) {
    return (
      <div>
        <RestoreIcon />
        <ConfirmationDeleteDialog
          dialogText={`By clicking ' OK ' you will restore this user.
           If you are not sure please fall back and think again.`}
          open={openDialog}
          close={toggleDialog}
          onSubmit={() => restoreUser()}
        />
      </div>
    )
  } else {
    return (
      <div>
        <DeleteIcon />
        <ConfirmationDeleteDialog
          dialogText={`By clicking ' OK ' you will mark this user as deleted.
           If you are not sure please fall back and think again.`}
          open={openDialog}
          close={toggleDialog}
          onSubmit={() => markUserAsDeleted()}
        />
      </div>
    )
  }
}

const showButtons = (
  showAddToExploreButton,
  goToUser,
  addToExplore,
  openDialog,
  toggleDialog
) => {
  if (showAddToExploreButton) {
    return (
      <ListItemIcon>
        <ConfirmationAddToExploreDialog
          dialogText={`By clicking 'OK' you will add this user to the featured users list.
           If you are not sure please fall back and think again.`}
          newDialogOpen={openDialog}
          toggleDialog={toggleDialog}
          onSubmit={addToExplore}
        />
      </ListItemIcon>
    )
  } else {
    return (
      <ListItemIcon>
        <IconButton onClick={() => goToUser()}>
          <Tooltip title="Go to User" placement="bottom">
            <LaunchIcon className={classes.color} />
          </Tooltip>
        </IconButton>
      </ListItemIcon>
    )
  }
}

User.propTypes = {
  user: PropTypes.object.isRequired,
  toggleDialog: PropTypes.func.isRequired,
  toggleAddToExploreDialog: PropTypes.func,
  newDialogOpen: PropTypes.bool.isRequired,
  addToExploreDialogOpen: PropTypes.bool,
  markUserAsDeleted: PropTypes.func.isRequired,
  restoreUser: PropTypes.func.isRequired,
  showAddToExploreButton: PropTypes.bool,
  addToExplore: PropTypes.func,
  goToUser: PropTypes.func.isRequired
}

export default User
