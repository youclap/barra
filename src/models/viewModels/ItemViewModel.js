export class ItemViewModel {
  constructor(item) {
    this.id = item.itemID
    this.type = item.itemType
  }
}
