import React from 'react'
import PropTypes from 'prop-types'
import PostTitle from 'components/Post/PostTitle'
import { showDelete } from '../Comment'
import Card from '@material-ui/core/Card'
import CardContent from '@material-ui/core/CardContent'
import Typography from '@material-ui/core/Typography'
import IconButton from '@material-ui/core/IconButton'
import classes from './CommentItem.scss'

const CommentItem = ({ comment, goToComment, newDialogOpen, toggleDialog, markAsDeleted, restoreComment }) => (
  <div className={classes.item}>
    <Card className={classes.card}>
      <CardContent onClick={() => goToComment(comment)}>
        <Typography>
          <PostTitle id={comment.postID} className={classes.title}/>
        </Typography>
        <IconButton className={classes.icon} onClick={toggleDialog}>
          {showDelete(comment, newDialogOpen, toggleDialog, markAsDeleted, restoreComment)}
        </IconButton>
        <div className={classes.teste}>
          <Typography className={classes.message}>{comment.message}</Typography>
        </div>
        <Typography>{comment.date}</Typography>
      </CardContent>
    </Card>
  </div>
)

CommentItem.PropTypes = {
  comment: PropTypes.object.isRequired,
  goToComment: PropTypes.func.isRequired,
  newDialogOpen: PropTypes.func.isRequired,
  toggleDialog: PropTypes.func.isRequired,
  markAsDeleted: PropTypes.func.isRequired,
  restoreComment: PropTypes.func.isRequired
}

export default CommentItem
