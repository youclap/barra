import React from 'react'
import PropTypes from 'prop-types'
import User from 'components/User'
import UserTab from '../UserTab'
import classes from './UserPage.scss'

const UserPage = ({ params: { id }, challenges, posts, comments }) => (
  <div className={classes.container}>
    <div className={classes.user}>
      <User id={id} showAddToExploreButton={true} />
    </div>
    <div className={classes.userTab}>
      <UserTab challenges={challenges} posts={posts} comments={comments} />
    </div>
  </div>
)

UserPage.propTypes = {
  params: PropTypes.object.isRequired,
  challenges: PropTypes.array.isRequired,
  posts: PropTypes.array.isRequired,
  comments: PropTypes.array.isRequired
}

export default UserPage
