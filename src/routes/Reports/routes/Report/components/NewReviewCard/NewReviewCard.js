import React from 'react'
import PropTypes from 'prop-types'
import Select from 'components/Select'
import { Field } from 'redux-form'
import TextField from '@material-ui/core/TextField'
import Card from '@material-ui/core/Card'
import CardContent from '@material-ui/core/CardContent'
import CardActions from '@material-ui/core/CardActions'
import Typography from '@material-ui/core/Typography'
import Button from '@material-ui/core/Button'
import classes from './NewReviewCard.scss'

const options = [
  {
    value: 'IGNORE',
    text: 'Ignore'
  },
  {
    value: 'DELETE',
    text: 'Delete'
  },
  {
    value: 'FORCE',
    text: 'Force Delete'
  }
]

const renderTextField = ({
  input,
  label,
  meta: { touched, error },
  ...custom
}) => (
  <TextField
    hintText={label}
    floatingLabelText={label}
    {...input}
    {...custom}
  />
)

const NewReviewCard = ({ review, handleSubmit }) => {
  console.log('review here ', review)
  return (
    <form onSubmit={handleSubmit}>
      <Card className={classes.card}>
        <CardContent>
          <Typography color="textSecondary">
            You were chosen to decide the fate of this post! 👩‍⚖️👨‍⚖️
          </Typography>
          <div>
            <Field
              name="value"
              component={props => {
                console.log('in select field with ', props)
                return (
                  <Select
                    title="select review value"
                    options={options}
                    onChange={props.input.onChange}
                    value={props.input.value}
                  />
                )
              }}
            />
          </div>
          <div>
            <Field
              name="information"
              component={renderTextField}
              label="Review information"
              placeholder="If you think it´s important to justify your choice please type here ✍"
              multiline
              rows={5}
            />
          </div>
        </CardContent>
        <CardActions>
          <Button onClick={handleSubmit} color="primary">
            Confirm
          </Button>
        </CardActions>
      </Card>
    </form>
  )
}

export default NewReviewCard
