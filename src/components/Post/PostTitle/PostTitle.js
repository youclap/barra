import React from 'react'
import PropTypes from 'prop-types'
import ChallengeTitle from 'components/Challenge/ChallengeTitle'
import classes from './PostTitle.scss'

const PostTitle = ({ id, post, className }) => {
    return <ChallengeTitle id={post.challengeID} className={classes.title} className={className} />
}

export default PostTitle
