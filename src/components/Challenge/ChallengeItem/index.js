import ChallengeItem from './ChallengeItem'
import enhance from '../Challenge.enhancer'
import enhancerItem from './ChallengeItem.enhancer'

export default enhance(enhancerItem(ChallengeItem))
