import { compose } from 'redux'
import { connect } from 'react-redux'
import { spinnerWhileLoading } from 'utils/components'
import { withHandlers, pure, withStateHandlers } from 'recompose'
import { withNotifications } from 'modules/notification'
import { ReviewViewModel } from 'models/viewModels/ReviewViewModel'
import { firestoreConnect } from 'react-redux-firebase'

const mapStateToProps = (state, ownProps) => {
  if (!ownProps || !ownProps.review) {
    return {}
  }
  const reviewViewModel = new ReviewViewModel(ownProps.review)
  return { review: reviewViewModel }
}

export default compose(
  connect(
    mapStateToProps,
    null
  ),
  spinnerWhileLoading(['review']),
  withNotifications,
  withStateHandlers(
    ({ initialDialogOpen = false }) => ({
      newDialogOpen: initialDialogOpen
    }),
    {
      toggleDialog: ({ newDialogOpen }) => () => ({
        newDialogOpen: !newDialogOpen
      })
    }
  ),
  firestoreConnect(),
  withHandlers({
    deleteReview: ({ firestore, review, showError, showSuccess }) => () => {
      return firestore
        .collection('review')
        .doc(review.id)
        .delete()
        .then(() => showSuccess('Deleted review with success!'))
        .catch(error => {
          console.error('delete review failed with error ', errror)
          return showError(error.message || 'cant delete review!')
        })
    }
  }),
  pure
)
