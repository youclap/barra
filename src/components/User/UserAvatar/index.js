import UserAvatar from './UserAvatar'
import enhance from 'components/User/User.enhancer'

export default enhance(UserAvatar)
