import React from 'react'
import { Link } from 'react-router'
import { REPORT_PATH } from 'constants'
import CardIcon from '../CardIcon/CardIcon'
import Card from '@material-ui/core/Card'
import CardContent from '@material-ui/core/CardContent'
import Typography from '@material-ui/core/Typography'
import Report from '@material-ui/icons/Report'
import classes from './ReportCardDashboard.scss'

const ReportCardDashboard = () => (
  <Link to={REPORT_PATH}>
    <div className={classes.main}>
      <Card className={classes.card}>
        <CardIcon Icon={Report} className={classes.cardIcon} />
        <CardContent>
          <Typography variant="headline" component="h2">
            Report
        </Typography>
        </CardContent>
      </Card>
    </div>
  </Link>
)

export default ReportCardDashboard
