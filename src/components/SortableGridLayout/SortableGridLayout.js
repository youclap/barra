import React from 'react'
import classes from './SortableGridLayout.scss'
import { SortableContainer, SortableElement } from 'react-sortable-hoc'
import FeaturedCard from 'routes/Explore/components/FeaturedCard'

const SortableItem = SortableElement(
  ({ item, onDelete }) => (
    <div className={classes.gridItem}>
      <FeaturedCard
        item={item}
        deleteFeaturedCard={onDelete}
      />
    </div>
  )
)

const SortableList = SortableContainer(
  ({ items, onDelete }) => (
    <div className={classes.grid}>
      {items.map((item, index) => (
        <SortableItem
          key={`item-${index}`}
          index={index}
          item={item}
          onDelete={onDelete}
        />
      ))}
    </div>
  )
)

export default SortableList
