export class UserReportViewModel {
  constructor(report) {
    this.reportID = report.id
    this.user = report.reporterID
    this.status = report.status
    this.value = report.value
    this.timestamp = report.timestamp.seconds
  }
}
