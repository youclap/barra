import React from 'react'
import { Link } from 'react-router'
import Button from '@material-ui/core/Button'
import { LOGIN_PATH } from 'constants'
import classes from './Navbar.scss'

export const LoginMenu = () => (
  <div className={classes.menu}>
    <Button className={classes.buttonStyle} component={Link} to={LOGIN_PATH}>
      Login
    </Button>
  </div>
)

export default LoginMenu
