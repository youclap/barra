import React from 'react'
import PropTypes from 'prop-types'
import ReactDrawer from '@material-ui/core/Drawer'
import MenuList from './MenuList'
import BurgerMenuButton from './BurgerMenuButton'

class Drawer extends React.Component {
  state = {
    left: false
  }

  toggleDrawer = (side, open) => () => {
    this.setState({
      [side]: open
    })
  }

  render() {
    return (
      <div>
        <BurgerMenuButton handleDrawerOpen={this.toggleDrawer('left', true)} />
        <ReactDrawer
          open={this.state.left}
          onClose={this.toggleDrawer('left', false)}
        >
          <div
            tabIndex={0}
            role="button"
            onClick={this.toggleDrawer('left', false)}
            onKeyDown={this.toggleDrawer('left', false)}
          >
            <MenuList />
          </div>
        </ReactDrawer>
      </div>
    )
  }
}

Drawer.propTypes = {
  goToCenas: PropTypes.func,
  goToHome: PropTypes.func,
  goToReports: PropTypes.func
}

export default Drawer
