import { REPORT_PATH as path } from 'constants'

export default store => ({
  path,
  getComponent(nextState, cb) {
    require.ensure(
      [],
      require => {
        const Reports = require('./components/ReportsPage').default

        cb(null, Reports)
      },
      'Reports'
    )
  }
})
