import React from 'react'
import PropTypes from 'prop-types'
import InputLabel from '@material-ui/core/InputLabel'
import MenuItem from '@material-ui/core/MenuItem'
import FormControl from '@material-ui/core/FormControl'
import ReactSelect from '@material-ui/core/Select'
import classes from './Select.scss'

class Select extends React.Component {
  state = {
    value: ''
  }

  handleChange = event => {
    this.setState({ value: event.target.value })
    this.props.onChange(event)
    this.props.value(event.target.value)
  }

  render() {
    return (
      <FormControl className={classes.container}>
        <InputLabel htmlFor="type-simple">{this.props.title}</InputLabel>
        <ReactSelect value={this.state.value} onChange={this.handleChange}>
          {this.props.options.map(option => (
            <MenuItem key={option.value} value={option.value}>
              {option.text}
            </MenuItem>
          ))}
        </ReactSelect>
      </FormControl>
    )
  }
}

Select.propTypes = {
  title: PropTypes.string,
  options: PropTypes.array.isRequired,
  onChange: PropTypes.func,
  value: PropTypes.string
}

export default Select
