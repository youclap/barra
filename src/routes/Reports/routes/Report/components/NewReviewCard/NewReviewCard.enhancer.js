import { reduxForm } from 'redux-form'
import { NEW_REVIEW_FORM_NAME } from 'constants'

export default reduxForm({
  form: NEW_REVIEW_FORM_NAME,
  onSubmitSuccess: (result, dispatch, props) => props.reset()
})