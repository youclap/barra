export class FeaturedChallengeViewModel {
    constructor(challenge) {
        this.id = challenge.uid
        this.title = challenge.title
        this.userID = challenge.userID
        this.path = 'challenge/' + challenge.uid + '/thumb_media'
    }
}