import { compose } from 'redux'
import { connect } from 'react-redux'
import { firestoreConnect } from 'react-redux-firebase'
import { spinnerWhileLoading } from 'utils/components'
import { pure } from 'recompose'

export default compose(
  // list of reports made by this user userID
  firestoreConnect(({ userID }) => {
    return [
      {
        collection: 'report',
        where: ['reporterID', '==', userID]
      }
    ]
  }),
  connect(({ firestore: { ordered } }) => {
    if (!ordered || !ordered.report) {
      return {}
    }
    const reportsMadeByUserCount = ordered.report.length
    console.log('number of reports made by this user: ', reportsMadeByUserCount)
    return { reportsCount: reportsMadeByUserCount }
  }),
  spinnerWhileLoading(['reportsCount']),
  pure
)
