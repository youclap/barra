import { CHALLENGE_PATH as path } from 'constants'

export default store => ({
  path,
  getComponent(nextState, cb) {
    require.ensure(
      [],
      require => {
        const Challenges = require('./components/ChallengesPage').default

        cb(null, Challenges)
      },
      'Challenges'
    )
  }
})
