import React from 'react'

export const AccessDenied = () => (
    <div className="flex-column-center">
        <h1>Whoops! Access Denied!</h1>
        <p>You don't have access to this functionality</p>
        <div>
            <iframe src="https://giphy.com/embed/3ohzdYt5HYinIx13ji" width="100%" height="100%"></iframe>
        </div>
    </div>
)

export default AccessDenied
