import React from 'react'
import PropTypes from 'prop-types'
import UserItem from 'components/User/UserItem'
import FileStorage from 'modules/fileStorage'
import EditChallengeDialog from './EditChallengeDialog'
import ConfirmationDeleteDialog from 'components/ConfirmationDeleteDialog'
import ConfirmationAddToExploreDialog from 'components/ConfirmationAddToExploreDialog'
import Card from '@material-ui/core/Card'
import CardContent from '@material-ui/core/CardContent'
import Typography from '@material-ui/core/Typography'
import Grid from '@material-ui/core/Grid'
import Tooltip from '@material-ui/core/Tooltip'
import List from '@material-ui/core/List'
import ListItem from '@material-ui/core/ListItem'
import ListItemIcon from '@material-ui/core/ListItemIcon'
import ListItemText from '@material-ui/core/ListItemText'
import Face from '@material-ui/icons/Face'
import IconButton from '@material-ui/core/IconButton'
import DeleteIcon from '@material-ui/icons/Delete'
import LaunchIcon from '@material-ui/icons/Launch'
import RestoreIcon from '@material-ui/icons/Restore'
import classes from './Challenge.scss'

const Challenge = ({
  challenge,
  goToUser,
  goToChallenge,
  newDialogOpen,
  toggleDialog,
  markChallengeAsDeleted,
  restoreChallenge,
  showAddToExploreButton,
  addToExploreDialogOpen,
  toggleAddToExploreDialog,
  addToExplore
}) => (
    <Card className={classes.card}>
      <FileStorage
        classes={classes.media}
        path={challenge.path}
        mediaType={challenge.mediaType}
        id={challenge.id}
      />
      <CardContent>
        <Grid container spacing={16} className={classes.grid}>
          <Grid className={classes.titleGrid}>
            <Typography gutterBottom variant="headline" component="h2">
              {challenge.title}
            </Typography>
          </Grid>
          <Grid className={classes.iconGrid}>
            <IconButton component={EditChallengeDialog} />
            <IconButton className={classes.icon} onClick={toggleDialog}>
              {showDelete(
                challenge,
                newDialogOpen,
                toggleDialog,
                markChallengeAsDeleted,
                restoreChallenge
              )}
            </IconButton>
            {showButtons(
              showAddToExploreButton,
              goToChallenge,
              addToExplore,
              addToExploreDialogOpen,
              toggleAddToExploreDialog
            )}
          </Grid>
        </Grid>
        <Typography component="p">{challenge.description}</Typography>
        <List>
          <ListItem>
            <Face />
            <UserItem id={challenge.userID} goToUser={goToUser} />
            <ListItemText primary={challenge.startDate} secondary="StartDate" />
            <ListItemText primary={challenge.endDate} secondary="EndDate" />
          </ListItem>
        </List>
      </CardContent>
    </Card>
  )

export const showDelete = (
  challenge,
  openDialog,
  toggleDialog,
  markAsDeleted,
  restoreChallenge
) => {
  if (challenge.isDeleted) {
    return (
      <div>
        <Tooltip title="Restore Challenge" placement="bottom">
          <RestoreIcon className={classes.color} />
        </Tooltip>
        <ConfirmationDeleteDialog
          dialogText={
            "By clicking 'OK' you will restore this challenge. If you are not sure please fall back and think again."
          }
          open={openDialog}
          onRequestClose={() => toggleDialog}
          onSubmit={() => restoreChallenge()}
        />
      </div>
    )
  } else {
    return (
      <div>
        <Tooltip title="Delete Challenge" placement="bottom">
          <DeleteIcon className={classes.color} />
        </Tooltip>
        <ConfirmationDeleteDialog
          dialogText={
            "By clicking 'OK' you will delete this challenge. If you are not sure please fall back and think again."
          }
          open={openDialog}
          onRequestClose={() => toggleDialog}
          onSubmit={() => markAsDeleted()}
        />
      </div>
    )
  }
}

const showButtons = (
  showAddToExploreButton,
  goToChallenge,
  addToExplore,
  openDialog,
  toggleDialog
) => {
  if (showAddToExploreButton) {
    return (
      <ListItemIcon>
        <ConfirmationAddToExploreDialog
          dialogText={
            "By clicking 'OK' you will add this challenge to the featured challenge list. If you are not sure please fall back and think again."
          }
          newDialogOpen={openDialog}
          toggleDialog={toggleDialog}
          onSubmit={addToExplore}
        />
      </ListItemIcon>
    )
  } else {
    return (
      <ListItemIcon>
        <IconButton onClick={() => goToChallenge()}>
          <Tooltip title="Go to Challenge" placement="bottom">
            <LaunchIcon className={classes.color} />
          </Tooltip>
        </IconButton>
      </ListItemIcon>
    )
  }
}

Challenge.propTypes = {
  challenge: PropTypes.object.isRequired,
  goToUser: PropTypes.func.isRequired,
  goToChallenge: PropTypes.func.isRequired,
  markChallengeAsDeleted: PropTypes.func.isRequired,
  restoreChallenge: PropTypes.func.isRequired,
  newDialogOpen: PropTypes.bool.isRequired,
  newDialogAddToExplore: PropTypes.bool.isRequired,
  toggleDialog: PropTypes.func.isRequired,
  toggleAddToExploreDialog: PropTypes.func
}

export default Challenge
