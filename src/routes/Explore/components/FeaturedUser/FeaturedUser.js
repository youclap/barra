import React from 'react'
import PropTypes from 'prop-types'
import UserAvatar from 'components/User/UserAvatar'
import ConfirmationDeleteDialog from 'components/ConfirmationDeleteDialog'
import Card from '@material-ui/core/Card'
import CardContent from '@material-ui/core/CardContent'
import CardActions from '@material-ui/core/CardActions'
import Typography from '@material-ui/core/Typography'
import DeleteIcon from '@material-ui/icons/Delete'
import IconButton from '@material-ui/core/IconButton'
import classes from './FeaturedUser.scss'

const FeaturedUser = ({ item, user, goToUser, deleteFeaturedCard, newDialogOpen, toggleDialog }) => (
  <Card className={classes.card}>
    <CardContent className={classes.cardContent}>
      <div className={classes.avatar}>
        <UserAvatar id={user.id} styles={classes} />
      </div>
      <Typography
        className={classes.titleUsername}
        onClick={() => goToUser(user)}>
        {user.username}
      </Typography>
    </CardContent>
    <CardActions>
      <IconButton className={classes.iconButton} onClick={toggleDialog}>
        <DeleteIcon fontSize="small" />
        <ConfirmationDeleteDialog
          dialogText={"By clicking ' OK ' you will remove this user from Featured Users list . If you are not sure please fall back and think again."}
          open={newDialogOpen}
          close={toggleDialog}
          onSubmit={() => deleteFeaturedCard(item)}
        />
      </IconButton>
    </CardActions>
  </Card>
)

FeaturedUser.propTypes = { 
  id: PropTypes.object.isRequired,
  item: PropTypes.object.isRequired,
  user: PropTypes.object.isRequired,
  goToUser: PropTypes.object.isRequired,
  deleteFeaturedCard: PropTypes.func.isRequired,
  newDialogOpen: PropTypes.func.IsRequired,
  toggleDialog: PropTypes.func.IsRequired
}

export default FeaturedUser
