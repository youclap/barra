import React from 'react'
import PropTypes from 'prop-types'
import FileStorage from 'modules/fileStorage'
import UserAvatar from 'components/User/UserAvatar'
import UserUsername from 'components/User/UserUsername'
import ConfirmationDeleteDialog from 'components/ConfirmationDeleteDialog'
import Card from '@material-ui/core/Card'
import CardHeader from '@material-ui/core/CardHeader'
import CardContent from '@material-ui/core/CardContent'
import Typography from '@material-ui/core/Typography'
import IconButton from '@material-ui/core/IconButton'
import DeleteIcon from '@material-ui/icons/Delete'
import classes from './FeaturedChallenge.scss'

const FeaturedChallenge = ({ item, challenge, goToChallenge, deleteFeaturedCard, newDialogOpen, toggleDialog }) => (
  <Card className={classes.card}>
    <CardHeader
      avatar={<UserAvatar id={challenge.userID} styles={classes} />}
      action={
        <IconButton className={classes.iconButton} onClick={toggleDialog}>
          <DeleteIcon />
          <ConfirmationDeleteDialog
            dialogText={"By clicking ' OK ' you will remove this challenge from Featured Challenges list. If you are not sure please fall back and think again."}
            open={newDialogOpen}
            close={toggleDialog}
            onSubmit={() => deleteFeaturedCard(item)} />
        </IconButton>}
      title={<UserUsername id={challenge.userID} />}>
    </CardHeader>
    <FileStorage className={classes.media} path={challenge.path} mediaType={"img"} id={challenge.id} />
    <CardContent>
      <Typography
        className={classes.titleChallenge}
        color="text"
        onClick={() => goToChallenge(challenge)}
      >
        {challenge.title}
      </Typography>
    </CardContent>
  </Card>
)

FeaturedChallenge.propTypes = {
  item: PropTypes.object.isRequired,
  challenge: PropTypes.object.isRequired,
  goToChallenge: PropTypes.func.isRequired,
  deleteFeaturedCard: PropTypes.func.isRequired,
  newDialogOpen: PropTypes.bool.isRequired,
  toggleDialog: PropTypes.func.isRequired
}

export default FeaturedChallenge
