import React from 'react'
import PropTypes from 'prop-types'
import FileStorage from 'modules/fileStorage'
import ChallengeTitle from 'components/Challenge/ChallengeTitle'
import { showDelete } from '../Post'
import Card from '@material-ui/core/Card'
import CardContent from '@material-ui/core/CardContent'
import IconButton from '@material-ui/core/IconButton';
import Typography from '@material-ui/core/Typography'
import classes from './PostItem.scss'

const PostItem = ({ post, goToPost, newDialogOpen, toggleDialog, markPostAsDeleted, restorePost }) => (
  <div className={classes.item}>
    <Card className={classes.card}>
      {createPostCard(post)}
      <div className={classes.color}>
        <CardContent>
          <IconButton className={classes.icon}  onClick={toggleDialog}>
            {showDelete(post, newDialogOpen, toggleDialog, markPostAsDeleted, restorePost)}
          </IconButton>
          <ChallengeTitle id={post.challengeID} className={classes.title} />
        </CardContent>
      </div>
    </Card>
  </div>
)

const createPostCard = (post) => {
  if (post.text) {
    return (
      <Card className={classes.cardText}>
        <Typography className={classes.text}>
          {post.text.value}
        </Typography>
      </Card>
    )
  } else {
    return <FileStorage path={post.path} mediaType={'img'} id={post.id} className={classes.media} />
  }
}

PostItem.PropTypes = {
  post: PropTypes.object.isRequired,
  goToPost: PropTypes.func.isRequired,
  newDialogOpen: PropTypes.func.isRequired,
  toggleDialog: PropTypes.func.isRequired,
  markPostAsDeleted: PropTypes.func.isRequired,
  restorePost: PropTypes.func.isRequired
}

export default PostItem
