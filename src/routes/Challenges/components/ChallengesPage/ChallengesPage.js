import React from 'react'
import ChallengesTable from '../ChallengesTable'
import classes from './ChallengesPage.scss'

const ChallengesPage = () => (
  <div>
    <h1 className={classes.title}> 💂⚔ Clap Arena ⚔💂</h1>
    <ChallengesTable />
  </div>
)

export default ChallengesPage
