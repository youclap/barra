import { reduxForm } from 'redux-form'
import { NEW_FEATURED_CARD } from 'constants'

export default reduxForm({
  form: NEW_FEATURED_CARD,
  onSubmitSuccess: (result, dispatch, props) => props.reset()
})
