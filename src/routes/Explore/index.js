import { EXPLORE_PATH as path } from 'constants'

export default store => ({
  path,

  getComponent(nextState, cb) {
    require.ensure(
      [],
      require => {
        const Explore = require('./components/ExplorePage').default

        cb(null, Explore)
      },
      'Explore'
    )
  }
})
