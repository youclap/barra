import moment from 'moment'

export const challengeIsActive = (challenge) => {
    const startDateMoment = moment.unix(challenge.startDate.seconds)
    const endDateMoment = moment.unix(challenge.endDate.seconds)

    const momentNow = moment()
    return momentNow.diff(startDateMoment) >= 0 &&
        endDateMoment.diff(momentNow) >= 0
}

export const challengeIsPublic = (challenge) => {
    return challenge.privacy.type === 'PUBLIC'
}