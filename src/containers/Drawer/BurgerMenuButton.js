import React from 'react'
import PropTypes from 'prop-types'
import IconButton from '@material-ui/core/IconButton'
import MenuIcon from '@material-ui/icons/Menu'

const BurgerMenuButton = ({ handleDrawerOpen }) => (
  <IconButton
    color="inherit"
    aria-label="Open drawer"
    onClick={handleDrawerOpen}>
    <MenuIcon />
  </IconButton>
)

BurgerMenuButton.propTypes = {
  handleDrawerOpen: PropTypes.func
}

export default BurgerMenuButton
