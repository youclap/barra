import { connect } from 'react-redux'
import { compose } from 'redux'
import { spinnerWhileLoading } from 'utils/components'
import { UserPagePostViewModel } from 'models/viewModels/UserPagePostViewModel'

export default compose(
    connect(({ firestore: { data } }, { id }) => {
        if (!data || !data.post || !id) {
            return {}
        }
        const userPostViewModel = new UserPagePostViewModel(data.post[id])
        return { post: userPostViewModel }
    }),
    spinnerWhileLoading(['post'])
)
