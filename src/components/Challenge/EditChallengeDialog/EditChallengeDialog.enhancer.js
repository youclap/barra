import { reduxForm } from 'redux-form'
import { CHANGE_CHALLENGE_INFO } from 'constants'
import { isEndDateValid } from 'utils/form'

export default reduxForm({
  form: CHANGE_CHALLENGE_INFO,
  initialValues: {
    challengeTitle: 'smack cam challenge',
    messageDescription: ' Play with your friends',
    startDate: '2019-05-12T10:30',
    endDate: '2020-02-20T16:12'
  },
  validate: (values, props) => {
    const dateValidation = isEndDateValid(values.startDate, values.endDate)
    return { endDate: dateValidation }
  },
  onSubmitSuccess: (result, dispatch, props) => props.reset()
})
