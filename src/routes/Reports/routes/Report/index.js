import { REPORT_PATH } from 'constants'

export default store => ({
  path: `${REPORT_PATH}/:id`,
  getComponent(nextState, cb) {
    require.ensure(
      [],
      require => {
        const ReportPage = require('./components/ReportPage').default

        cb(null, ReportPage)
      },
      'Report'
    )
  }
})
