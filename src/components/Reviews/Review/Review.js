import React from 'react'
import PropTypes from 'prop-types'
import UserItem from 'components/User/UserItem'
import ConfirmationDeleteDialog from 'components/ConfirmationDeleteDialog'
import Card from '@material-ui/core/Card'
import IconButton from '@material-ui/core/IconButton'
import ListItem from '@material-ui/core/ListItem'
import ListItemText from '@material-ui/core/ListItemText'
import Tooltip from '@material-ui/core/Tooltip'
import DeleteIcon from '@material-ui/icons/Delete'
import classes from './../Review.scss'

const Review = ({ review, deleteReview, newDialogOpen, toggleDialog }) => (
  <Card className={classes.reviewCard}>
    <ListItem>
      <UserItem id={review.userID} />
      <ListItemText primary={review.date} secondary="Date" />
      <ListItemText primary={review.value} secondary="Voted" />
    </ListItem>
    <ListItem>
      <ListItemText primary={review.information} />
    </ListItem>
    <ListItem>
      <IconButton onClick={toggleDialog}>
        <Tooltip title="Delete Review" placement="bottom">
          <DeleteIcon className={classes.color} />
        </Tooltip>
        <ConfirmationDeleteDialog
          dialogText={
            "By clicking 'OK' you will delete this review. If you are not sure please fall back and think again."
          }
          open={newDialogOpen}
          onRequestClose={() => toggleDialog}
          onSubmit={() => deleteReview()}
        />
      </IconButton>
    </ListItem>
  </Card>
)

Review.propTypes = {
  review: PropTypes.object.isRequired,
  deleteReview: PropTypes.func
}

export default Review
