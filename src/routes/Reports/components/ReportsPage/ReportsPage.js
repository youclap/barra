import React from 'react'
import classes from './ReportsPage.scss'
import ReportTable from '../ReportTable'

export const ReportsPage = () => (
  <div className={classes.container}>
    <h1 className={classes.title}>🏛👨‍⚖️ Clap Court 👩‍⚖️🏛 </h1>
    <div className={classes.reportTable}>
      <ReportTable />
    </div>
  </div>
)

export default ReportsPage
