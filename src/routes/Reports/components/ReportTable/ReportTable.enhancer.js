import { compose } from 'redux'
import { connect } from 'react-redux'
import { withHandlers, withStateHandlers, pure } from 'recompose'
import { firestoreConnect } from 'react-redux-firebase'
import { withRouter, spinnerWhileLoading } from 'utils/components'
import { REPORT_PATH } from 'constants'
import { ReportTableRowViewModel } from 'models/viewModels/ReportTableRowViewModel'
import { ReportGroup } from 'models/ReportGroup'
import { UserReportViewModel } from 'models/viewModels/UserReportViewModel'
import { compareNumbers, compareStrings, diffMoments } from 'utils/compare'

function groupReportsByItemID(reports) {
  const groupedReportsObject = groupBy(reports, 'itemID')
  return Object.keys(groupedReportsObject).map(key => {
    const groupedReport = groupedReportsObject[key]
    const reports = groupedReport.map(report => new UserReportViewModel(report))

    return new ReportGroup(
      groupedReport[0].id,
      key,
      groupedReport[0].itemType,
      reports
    )
  })
}

const groupBy = (groups, key) => {
  return groups.reduce((groups, item) => {
    const val = item[key]
    groups[val] = groups[val] || []
    groups[val].push(item)
    return groups
  }, {})
}

export default compose(
  firestoreConnect(['report']),
  connect(({ firestore: { ordered } }) => {
    if (!ordered.report) {
      return {}
    }

    const reports = ordered.report

    const tableHeadCells = [
      { id: 'itemID', numeric: false, disablePadding: false, label: 'Item ID' },
      { id: 'itemType', numeric: false, disablePadding: false, label: 'Type' },
      {
        id: 'lastReportTimestamp',
        numeric: false,
        disablePadding: false,
        label: 'Last report date'
      },
      {
        id: 'reportsCount',
        numeric: true,
        disablePadding: false,
        label: 'Total number of reports'
      },
      {
        id: 'openReportsCount',
        numeric: true,
        disablePadding: false,
        label: 'To resolve'
      },
      {
        id: 'status',
        numeric: true,
        disablePadding: false,
        label: 'Status'
      }
    ]

    const groupedReports = groupReportsByItemID(reports)
    const reportsViewModel = groupedReports.map(
      report => new ReportTableRowViewModel(report)
    )

    const itemReportMap = new Map(
      groupedReports.map(report => [report.itemID, report.reportID])
    )
    return {
      reports: reportsViewModel,
      tableHeadCells: tableHeadCells,
      itemReport: itemReportMap
    }
  }),
  spinnerWhileLoading(['reports', 'itemReport', 'tableHeadCells']),
  withRouter,
  withStateHandlers(
    ({ initialOrder = 'desc', reports }) => {
      return {
        order: initialOrder,
        orderBy: 'lastReportTimestamp',
        reports: sortArray(reports, initialOrder, 'lastReportTimestamp')
      }
    },
    {
      handleRequestSort: (state, { reports }) => (event, property) => {
        return {
          order:
            state.orderBy === property && state.order === 'desc'
              ? 'asc'
              : 'desc',
          orderBy: property,
          reports: sortArray(reports, state.order, property)
        }
      }
    }
  ),
  withHandlers({
    goToReportID: ({ itemReport, router }) => itemID => {
      const reportID = itemReport.get(itemID)
      router.push({
        pathname: `${REPORT_PATH}/${reportID}`
      })
    }
  }),
  pure
)

const sortArray = (array, order, orderBy) => {
  return array.sort(getSort(order, orderBy))
}

const compare = (a, b, orderBy) => {
  if (orderBy === 'itemID' || orderBy === 'itemType' || orderBy === 'status') {
    return compareStrings(a, b, orderBy)
  } else if (orderBy === 'lastReportTimestamp') {
    return diffMoments(a, b, orderBy)
  } else {
    return compareNumbers(a, b, orderBy)
  }
}

const getSort = (order, orderBy) => {
  return order === 'asc'
    ? (a, b) => compare(a, b, orderBy)
    : (a, b) => -compare(a, b, orderBy)
}
