import { compose } from 'redux'
import { connect } from 'react-redux'
import { spinnerWhileLoading } from 'utils/components'
import { FeaturedUserViewModel } from 'models/viewModels/FeaturedUserViewModel'

export default compose(
    connect(({ firestore: { data: { user } } }, { id }) => {
        if (!user || !user[id]) {
            return {}
        }

        return { user: new FeaturedUserViewModel(user[id]) }
    }),
    spinnerWhileLoading(['user'])
)
