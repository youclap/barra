import React from 'react'
import PropTypes from 'prop-types'
import FileStorage from 'modules/fileStorage'
import UserItem from 'components/User/UserItem'
import AdditionalInfo from './AdditionalInfo'
import ConfirmationDeleteDialog from 'components/ConfirmationDeleteDialog'
import Button from '@material-ui/core/Button'
import Card from '@material-ui/core/Card'
import CardContent from '@material-ui/core/CardContent'
import ListItemIcon from '@material-ui/core/ListItemIcon'
import Typography from '@material-ui/core/Typography'
import List from '@material-ui/core/List'
import ListItem from '@material-ui/core/ListItem'
import ListItemText from '@material-ui/core/ListItemText'
import IconButton from '@material-ui/core/IconButton'
import DeleteIcon from '@material-ui/icons/Delete'
import LaunchIcon from '@material-ui/icons/Launch'
import RestoreIcon from '@material-ui/icons/Restore'
import Face from '@material-ui/icons/Face'
import classes from './Post.scss'

const Post = ({ post, goToUser, newDialogOpen, toggleDialog, newAdditionalInfoDialog, toggleAdditionalInfoDialog, markPostAsDeleted, restorePost, goToPost }) => (
  <Card className={classes.card}>
    {createPostCard(post)}
    <CardContent>
      <List>
        <ListItem>
          <Face />
          <UserItem id={post.userID} goToUser={goToUser} />
          <ListItemText primary={post.date} secondary="Date" />
          <ListItemText primary={post.claps} secondary="Claps" />
          <ListItemText>
            <Button onClick={toggleAdditionalInfoDialog}>Additional info</Button>
            <AdditionalInfo
              challenge={post.challengeID}
              open={newAdditionalInfoDialog}
              onRequestClose={toggleAdditionalInfoDialog} />
          </ListItemText>
          <ListItemIcon>
            <IconButton onClick={() => goToPost(post)}>
              <LaunchIcon />
            </IconButton>
          </ListItemIcon>
          <IconButton onClick={toggleDialog}>
            {showDelete(post, newDialogOpen, toggleDialog, markPostAsDeleted, restorePost)}
          </IconButton>
        </ListItem>
      </List>
    </CardContent>
  </Card>
)

export const showDelete = (post, newDialogOpen, toggleDialog, markAsDeleted, restorePost) => {
  if (post.isDeleted) {
    return (
      <div>
        <RestoreIcon />
        <ConfirmationDeleteDialog
          dialogText={"By clicking 'OK' you will restore this post. If you are not sure please fall back and think again."}
          open={newDialogOpen}
          onRequestClose={() => toggleDialog}
          onSubmit={() => restorePost()} />
      </div>
    )
  } else {
    return (
      <div>
        <DeleteIcon />
        <ConfirmationDeleteDialog
          dialogText={"By clicking 'OK' you will delete this post. If you are not sure please fall back and think again."}
          open={newDialogOpen}
          onRequestClose={() => toggleDialog}
          onSubmit={() => markAsDeleted()} />
      </div>
    )
  }
}

const createPostCard = (post) => {
  if (post.text) {
    return (
      <Card
        className={classes.text}
        style={{
          background: `linear-gradient(to right , ${post.text.colors[0]},${
            post.text.colors[1]
            })`
        }}
      >
        <Typography className={classes.typography} style={{ color: 'white' }}>
          {post.text.value}
        </Typography>
      </Card>
    )
  } else {
    return <FileStorage path={post.path} mediaType={post.mediaType} id={post.id} />
  }
}

Post.propTypes = {
  post: PropTypes.object.isRequired,
  goToUser: PropTypes.func.isRequired,
  markPostAsDeleted: PropTypes.func.isRequired,
  restorePost: PropTypes.func.isRequired,
  goToPost: PropTypes.func.IsRequired,
  newDialogOpen: PropTypes.bool.isRequired,
  toggleDialog: PropTypes.func.isRequired,
  newAdditionalInfoDialog: PropTypes.bool.isRequired,
  toggleAdditionalInfoDialog: PropTypes.func.isRequired
}

export default Post
