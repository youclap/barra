import NewFeaturedCardDialog from './NewFeaturedCardDialog'
import enhance from './NewFeaturedCardDialog.enhancer'

export default enhance(NewFeaturedCardDialog)
