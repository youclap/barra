import * as actions from './actions'
import enhance from './FileStorage.enhancer'
import FileStorage from './FileStorage'

export reducer from './reducer'
export default enhance(FileStorage)
