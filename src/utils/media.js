export const getHTMLElement = mediaType => {
  if (mediaType === 'PHOTO') return 'img'
  else if (mediaType === 'VIDEO') return 'video'
}

export const isTextPost = mediaType => {
  if (mediaType === 'TEXT') {
    return true
  }
  return false
}

export const createPostTextObject = post => ({
  value: post.media.value,
  colors: post.media.colors
})