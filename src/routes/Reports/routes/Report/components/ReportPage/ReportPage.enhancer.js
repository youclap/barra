import { compose } from 'redux'
import { connect } from 'react-redux'
import { firestoreConnect, withFirebase, firebaseConnect } from 'react-redux-firebase'
import { spinnerWhileLoading } from 'utils/components'
import { withHandlers, pure } from 'recompose'
import { withNotifications } from 'modules/notification'
import { ItemViewModel } from 'models/viewModels/ItemViewModel'

export default compose(
  firestoreConnect(({ params }) => [
    {
      collection: 'report',
      doc: params.id
    }
  ]),
  connect(({ firebase: { auth }, firestore: { data } }, { params }) => {
    if (!data.report || !data.report[params.id]) {
      return {}
    }
    const report = data.report[params.id]
    const item = new ItemViewModel(report)
    return { item: item, auth: auth }
  }),
  spinnerWhileLoading(['item']),
  withNotifications,
  withHandlers({
    addReview: ({ auth, firestore, item, showError, showSuccess, params }) => newInstance => {
      const reviewValue = newInstance.value
      const reviewFeedback = newInstance.information

      const review = {
        itemID: item.id,
        itemType: item.type,
        value: reviewValue,
        information: reviewFeedback,
        reportID: params.id,
        createdBy: auth.uid,
        createdAt: firestore.FieldValue.serverTimestamp()
      }
      return firestore
        .collection('review')
        .add(review)
        .then(() => showSuccess('Added Report Review successfully'))
        .catch(error => {
          console.error('Failed to create review with error ', error)
          return showError(error.message || 'Could not add review')
        })
    }
  }),
  pure
)
