import { compose } from 'redux'
import { connect } from 'react-redux'
import { withHandlers, withStateHandlers, pure } from 'recompose'
import { firestoreConnect } from 'react-redux-firebase'
import { spinnerWhileLoading, withRouter } from 'utils/components'
import { withNotifications } from 'modules/notification'
import { PostViewModel } from 'models/viewModels/PostViewModel'
import { POST_PATH } from 'constants'
import { updateDocumentField } from 'utils/updateField'

export default compose(
  firestoreConnect(({ id }) => [
    {
      collection: 'post',
      doc: id
    }
  ]),
  connect(({ firestore: { data } }, { id }) => {
    if (!data.post || !data.post[id]) {
      return {}
    }
    const post = data.post[id]
    const postViewModel = new PostViewModel(post)
    return { post: postViewModel }
  }),
  spinnerWhileLoading(['post']),
  withNotifications,
  withRouter,
  withStateHandlers(
    ({ initialDialogOpen = false }) => ({
      newDialogOpen: initialDialogOpen,
      newAdditionalInfoDialog: initialDialogOpen

    }),
    {
      toggleDialog: ({ newDialogOpen }) => () => ({
        newDialogOpen: !newDialogOpen
      }),
      toggleAdditionalInfoDialog: ({ newAdditionalInfoDialog }) => () => ({
        newAdditionalInfoDialog: !newAdditionalInfoDialog
      })
    }
  ),
  withHandlers({
    markPostAsDeleted: ({ firestore, post, showSuccess, showError }) => () => {
      return updateDocumentField(firestore, 'post', post, 'deleted', true)
        .then(() => showSuccess('Post marked as deleted!'))
        .catch(error => {
          console.error(error.message)
          return showError('Failed marking post as deleted!')
        })
    },
    restorePost: ({ firestore, post, showSuccess, showError }) => () => {
      return updateDocumentField(firestore, 'post', post, 'deleted', false)
        .then(() => showSuccess('Post restored with success!'))
        .catch(error => {
          console.error(error.message)
          return showError('Failed restoring deleted post!')
        })
    },
    goToPost: props => post => {
      console.log('tapping post ', post, ' with props ', props)
      props.router.push({
        pathname: `${POST_PATH}/${post.id}`,
        state: post
      })
    }
  }),
  pure
)
