import {
  FILE_GET_URL,
  FILE_GET_URL_ERROR,
  FILE_GET_URL_CONTENTTYPE,
  FILE_GET_URL_CONTENTTYPE_ERROR
} from './actionTypes'

export const getFileURL = (path, id) => (dispatch, getState, getFirebase) => {
  return getFirebase()
    .getFileURL(path)
    .then(url => {
      const payload = {
        id: id,
        url: url
      }
      return dispatch({ type: FILE_GET_URL, payload })
    })
    .catch(error => {
      console.error('failed get gile url with error ', error)
      return dispatch({ type: FILE_GET_URL_ERROR, payload: undefined })
    })
}

export const getFileURLAndContentType = (path, id) => (
  dispatch,
  getState,
  getFirebase
) => {
  return getFirebase()
    .getFileURLAndContentType(path)
    .then(result => {
      console.log('result in get file url/content type ', result)
      const payload = {
        id: id,
        url: result.url,
        contentType: result.contentType
      }
      return dispatch({ type: FILE_GET_URL_CONTENTTYPE, payload })
    })
    .catch(error => {
      console.error(
        'failed getting file url and content type with error ',
        error
      )
      return dispatch({
        type: FILE_GET_URL_CONTENTTYPE_ERROR,
        payload: undefined
      })
    })
}
