import { combineReducers } from 'redux'
import { without, omit } from 'lodash'
import {
  FILE_GET_URL,
  FILE_GET_URL_ERROR
} from './actionTypes'

const fileStorage = (state = {}, action) => {
  switch (action.type) {
    case FILE_GET_URL:
      return action.payload
    case FILE_GET_URL_ERROR:
      return undefined
    default:
      return state
  }
}

const allIds = (state = [], action) => {
  switch (action.type) {
    case FILE_GET_URL:
      return [...state, action.payload.id]
    case FILE_GET_URL_ERROR:
      return without(state, action.payload)
    default:
      return state
  }
}

const byId = (state = {}, action) => {
  switch (action.type) {
    case FILE_GET_URL:
      return {
        ...state,
        [action.payload.id]: fileStorage(state[action.payload.id], action)
      }
    case FILE_GET_URL_ERROR:
      return omit(state, action.payload)
    default:
      return state
  }
}

export default combineReducers({ byId, allIds })
