import { compose } from 'redux'
import { connect } from 'react-redux'
import { firestoreConnect } from 'react-redux-firebase'
import { spinnerWhileLoading } from 'utils/components'
import { pure } from 'recompose'

export default compose(
    firestoreConnect(({ item }) => [
        {
            collection: 'review',
            where: ['itemID', '==', item.id],
            orderBy: ['createdAt', 'asc']
        }
    ]),
    connect(({ firestore: { ordered } }) => {
        if (!ordered) {
            return {}
        }
        return { reviews: ordered.review }
    }),
    spinnerWhileLoading(['reviews']),
    pure
)
