import { getDocument, documentIsAvailable } from 'utils/firestore'
import { challengeIsActive, challengeIsPublic } from 'utils/challenge'

export const addDocumentToExplore = (firestore, value, type, order) => {
    return validateDocument(firestore, value, type)
        .then(result => {
            if (result) {
                return updateExploreDocuments(firestore, type, order)
            } else {
                console.error(type, '/', value, 'isn\'t valid!')
                return Promise.reject(new Error(type + '/' + value + ' isn\'t valid!'))
            }
        })
        .then(() => {
            return createExploreDocument(firestore, value, type, order)
        })
}

export const deleteDocumentFromExplore = (firestore, docID, type, order) => {
    return firestore
        .collection('explore')
        .doc(docID)
        .delete()
        .then(() => {
            console.log('document ', docID, 'deleted with success!')
            return updateExploreDocuments(firestore, type, order, -1)
        })
        .then(() => {
            return console.log('documents after order ', order, ' updated with success!')
        })
}

export const getFeaturedArrayByType = (array, type) => {
    return array.filter(document => (document.type === type && document.section === 'FEATURED'))
}

export const featuredUsersCount = (firestore) => {
    return firestore.collection('explore')
        .where('type', '==', 'USER')
        .where('section', '==', 'FEATURED')
        .get()
        .then(querySnapshot => querySnapshot.size)
}

export const featuredChallengesCount = (firestore) => {
    return firestore.collection('explore')
        .where('type', '==', 'CHALLENGE')
        .where('section', '==', 'FEATURED')
        .get()
        .then(querySnapshot => querySnapshot.size)
}

const validateDocument = (firestore, value, type) => {
    if (type === 'CHALLENGE') {
        return getDocument(firestore, type.toLowerCase(), value)
            .then(challenge => {
                if (challenge) {
                    return Promise.all([challengeIsActive(challenge), documentIsAvailable(challenge), challengeIsPublic(challenge)])
                } else {
                    console.error('challenge document', value, 'isn\'t valid!')
                    return null
                }
            })
            .then(result => {
                return result[0] && result[1] && result[2]
            })
    } else {
        return getDocument(firestore, type.toLowerCase(), value)
            .then(user => {
                if (user) {
                    return documentIsAvailable(user)
                } else {
                    console.error('user document ', value, 'isn\'t valid!')
                    return null
                }
            })
    }
}

const createExploreDocument = (firestore, value, type, order) => {
    return firestore
        .collection('explore')
        .add({
            type: type,
            section: 'FEATURED',
            order: order,
            value: value
        })
        .then(() => {
            console.log('doc added')
            return Promise.resolve(true)
        })
        .catch(error => {
            console.error('failed creating firestore document value ', value, ' with error ', error)
            return Promise.reject(error)
        })
}

const updateExploreDocuments = (firestore, type, startOrder, orderIncrement = 1) => {
    return firestore.collection('explore')
        .where('type', '==', type.toUpperCase())
        .where('section', '==', 'FEATURED')
        .where('order', '>=', parseInt(startOrder))
        .get()
        .then(snapshot => {
            return Promise.all(snapshot.docs.map(document => document.ref.update({
                order: document.data().order + orderIncrement
            })))
        })
        .then(() => {
            console.log('🚀 Explore challenges updated to new positions')
        })
        .catch(error => {
            console.error('💥 Failed to update explore challenges with error ', error)
        })
}