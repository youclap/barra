import React from 'react'
import { Link } from 'react-router'
import { CHALLENGE_PATH } from 'constants'
import CardIcon from '../CardIcon/CardIcon'
import Card from '@material-ui/core/Card'
import CardContent from '@material-ui/core/CardContent'
import Typography from '@material-ui/core/Typography'
import VideoCam from '@material-ui/icons/Videocam'
import classes from './ChallengesCardDashboard.scss'

const ChallengesCardDashboard = () => (
  <Link to={CHALLENGE_PATH}>
    <div className={classes.main}>
      <Card className={classes.card}>
        <CardIcon Icon={VideoCam} className={classes.cardIcon} />
        <CardContent>
          <Typography variant="headline" component="h2">
            Challenges
        </Typography>
        </CardContent>
      </Card>
    </div>
  </Link>
)

export default ChallengesCardDashboard
