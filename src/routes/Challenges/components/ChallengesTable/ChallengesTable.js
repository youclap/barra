import * as React from 'react'
import PropTypes from 'prop-types'
import ChallengeTableHead from './ChallengeTableHead'
import UserItem from 'components/User/UserItem'
import FileStorage from 'modules/fileStorage'
import { Waypoint } from 'react-waypoint'
import CircularProgress from '@material-ui/core/CircularProgress'
import Table from '@material-ui/core/Table'
import TableBody from '@material-ui/core/TableBody'
import TableCell from '@material-ui/core/TableCell'
import TableRow from '@material-ui/core/TableRow'
import Paper from '@material-ui/core/Paper'
import classes from './ChallengesTable.scss'

export const ChallengesTable = ({
  challenges,
  goToChallenge,
  tableHeadCells,
  order,
  orderBy,
  handleRequestSort,
  loadMoreChallenges,
  isLoading
}) => (
    <div>
      <p>challenge number {challenges.length}</p>
      <Paper className={classes.root}>
        <Table className={classes.table}>
          <ChallengeTableHead
            cells={tableHeadCells}
            order={order}
            orderBy={orderBy}
            onRequestSort={handleRequestSort}
          />
          <TableBody>
            {challenges.map(challenge => {
              return (
                <TableRow hover key={challenge.id} tabIndex={-1}>
                  <TableCell
                    className={classes.cellChallengeID}
                    component="th"
                    scope="row"
                    align="left"
                    onClick={() => goToChallenge(challenge.id)}
                  >
                    {challenge.id}
                  </TableCell>
                  <TableCell>
                    <FileStorage
                      classes={classes.thumbnail}
                      path={challenge.thumbnail}
                      mediaType={"img"}
                      id={challenge.id}
                    />
                  </TableCell>
                  <TableCell className={classes.cell}>
                    <UserItem id={challenge.userID} />
                  </TableCell>
                  <TableCell className={classes.cellTitle} align="left">
                    {challenge.title}
                  </TableCell>
                  <TableCell className={classes.cell}>
                    {challenge.startDate}
                  </TableCell>
                  <TableCell className={classes.cell}>
                    {challenge.endDate}
                  </TableCell>
                  <TableCell className={classes.cell}>{challenge.status}</TableCell>
                </TableRow>
              )
            })}
          </TableBody>
        </Table>
      </Paper>
      <div className={classes.progress}>
        {renderWaypoint(isLoading, loadMoreChallenges)}
        <CircularProgress />
      </div>
    </div>
  )

const renderWaypoint = (isLoading, loadMoreChallenges) => {
  if (!isLoading) {
    return (
      <Waypoint onEnter={loadMoreChallenges} />
    )
  }
}

ChallengesTable.propTypes = {
  challenges: PropTypes.array.isRequired
}

export default ChallengesTable
