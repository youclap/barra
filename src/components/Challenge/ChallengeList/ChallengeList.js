import React from 'react'
import PropTypes from 'prop-types'
import ChallengeItem from '../ChallengeItem'
import classes from './ChallengeList.scss'

const ChallengeList = ({ challenges }) => (
  <div className={classes.container}>
    {challenges.length > 0 ? (
      challenges.map(id => <ChallengeItem key={'challenge-' + id} id={id} />)
    ) : <p>0 challenges created 😞</p>
    }
  </div>
)

ChallengeList.propTypes = {
  challenges: PropTypes.array.isRequired
}

export default ChallengeList
