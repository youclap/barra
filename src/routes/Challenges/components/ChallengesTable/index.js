import ChallengesTable from './ChallengesTable'
import enhance from './ChallengesTable.enhancer'

export default enhance(ChallengesTable)