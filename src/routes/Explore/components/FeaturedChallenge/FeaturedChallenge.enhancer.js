import { compose } from 'redux'
import { connect } from 'react-redux'
import { spinnerWhileLoading } from 'utils/components'
import { FeaturedChallengeViewModel } from 'models/viewModels/FeaturedChallengeViewModel'

export default compose(
  connect(({ firestore: { data: { challenge } } }, { id }) => {
    if (!challenge || !challenge[id]) {
      return {}
    }
    
    return { challenge: new FeaturedChallengeViewModel(challenge[id]) }
  }),
  spinnerWhileLoading(['challenge'])
)
