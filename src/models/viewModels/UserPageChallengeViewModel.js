export class UserPageChallengeViewModel {
    constructor(challenge) {
        this.id = challenge.uid
        this.title = challenge.title
        this.isDeleted = challenge.deleted
        this.path = 'challenge/' + challenge.uid + '/thumb_media'
    }
}