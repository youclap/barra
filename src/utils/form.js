import moment from 'moment'
export const required = value => (value ? undefined : 'Required')

export const validateEmail = value =>
  value && !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}$/i.test(value)
    ? 'Invalid email address'
    : undefined

export const isEndDateValid = (startDate, endDate) => {
  const startMoment = moment(startDate)
  const endMoment = moment(endDate)

  if (endMoment.isAfter(startMoment)) {
    return undefined
  } else {
    return 'EndDate must be after startDate!'
  }
}
