import { get } from 'lodash'
import { connect } from 'react-redux'
import {
  withHandlers,
  compose,
  withProps,
  flattenProp,
  withStateHandlers
} from 'recompose'
import { withFirebase, isEmpty, isLoaded } from 'react-redux-firebase'
import { withRouter, spinnerWhileLoading } from 'utils/components'

export default compose(
  connect(({ firebase: { auth, profile } }) => ({
    auth,
    profile
  })),
  // Wait for auth to be loaded before going further
  spinnerWhileLoading(['profile']),
  withStateHandlers(
    ({ accountMenuOpenInitially = false }) => ({
      accountMenuOpen: accountMenuOpenInitially,
      anchorEl: null
    }),
    {
      closeAccountMenu: ({ accountMenuOpen }) => () => ({
        anchorEl: null
      }),
      handleMenu: () => event => ({
        anchorEl: event.target
      })
    }
  ),
  // Add props.router (used in handlers)
  withRouter,
  // Add props.firebase (used in handlers)
  withFirebase,
  // Handlers
  withHandlers({
    handleLogout: props => () => {
      props.firebase.logout()
      props.router.push('/')
      props.closeAccountMenu()
    }
  }),
  withProps(({ auth, profile }) => {
    return ({
      authExists: isLoaded(auth) && !isEmpty(auth),
      isAdmin: isLoaded(auth) && !isEmpty(auth) && get(profile, 'role') === 'admin'
    })
  }),
  // Flatten profile so that avatarUrl and displayName are available
  flattenProp('profile')
)
