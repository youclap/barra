import moment from 'moment'

const dateFormat = 'DD/MM/YYYY'

export class UserPageCommentViewModel {
    constructor(comment) {
        this.id = comment.uid
        this.message = comment.value
        this.date = moment.unix(comment.timestamp.seconds).format(dateFormat)
        this.isDeleted = comment.deleted
        this.postID = comment.postID
    }
}