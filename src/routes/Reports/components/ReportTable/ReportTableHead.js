import React from 'react'
import PropTypes from 'prop-types'
import TableHead from '@material-ui/core/TableHead'
import TableRow from '@material-ui/core/TableRow'
import TableCell from '@material-ui/core/TableCell'
import TableSortLabel from '@material-ui/core/TableSortLabel'
import Tooltip from '@material-ui/core/Tooltip'

const ReportTableHead = ({ order, orderBy, cells, onRequestSort }) => {

    const createSortHandler = property => event => {
        onRequestSort(event, property)
    }

    return (
        <TableHead>
            <TableRow>
                {cells.map(cell => (
                    <TableCell
                        key={cell.id}
                        align={cell.numeric ? 'right' : 'left'}
                        padding={cell.disablePadding ? 'none' : 'default'}
                        sortDirection={orderBy === cell.id ? order : false}>
                        <Tooltip
                            title='Sort'
                            placement={cell.numeric ? 'bottom-end' : 'bottom-start'}
                            enterDelay={300}>
                            <TableSortLabel
                                active={orderBy === cell.id}
                                direction={order}
                                onClick={createSortHandler(cell.id)}>
                                {cell.label}
                            </TableSortLabel>
                        </Tooltip>
                    </TableCell>
                ))}
            </TableRow>
        </TableHead>
    )
}

ReportTableHead.propTypes = {
    cells: PropTypes.array.isRequired,
    order: PropTypes.string.isRequired,
    orderBy: PropTypes.string.isRequired,
    onRequestSort: PropTypes.func
}

export default ReportTableHead
