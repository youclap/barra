/**
 * Update a document field on firestore db
 * @param {Object} firestore 
 * @param {String} collection 
 * @param {Object} document 
 * @param {String} key 
 * @param {Object} value 
 */
export const updateDocumentField = (firestore, collection, document, key, value) => {
    return firestore
        .collection(collection)
        .doc(document.id)
        .update(key, value)
        .catch(error => Promise.reject(error))
}