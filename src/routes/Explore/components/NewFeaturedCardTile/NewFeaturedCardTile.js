import React from 'react'
import PropTypes from 'prop-types'
import ContentAddCircle from '@material-ui/icons/AddCircle'
import classes from './NewFeaturedCardTile.scss'
import { IconButton } from '@material-ui/core'

export const NewFeaturedCardTile = ({ onClick }) => (
  <IconButton className={classes.iconButton} onClick={onClick}>
    <ContentAddCircle className={classes.icon} />
  </IconButton>
)

NewFeaturedCardTile.propTypes = {
  onClick: PropTypes.func
}

export default NewFeaturedCardTile
