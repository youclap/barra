import { connect } from 'react-redux'
import { compose } from 'redux'
import { spinnerWhileLoading } from 'utils/components'
import { UserPageCommentViewModel } from 'models/viewModels/UserPageCommentViewModel'

export default compose(
    connect(({ firestore: { data } }, { id }) => {
        if (!data || !data.comment) {
            return {}
        }
        const userCommentViewModel = new UserPageCommentViewModel(data.comment[id])
        return { comment: userCommentViewModel }
    }),
    spinnerWhileLoading(['comment'])
)
