export class FeaturedUserViewModel {
    constructor(user) {
        this.id = user.uid,
        this.username = user.username
    }
}