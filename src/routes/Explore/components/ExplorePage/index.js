import ExplorePage from './ExplorePage'
import enhance from './ExplorePage.enhancer'
import { UserIsAdmin } from 'utils/router'

export default UserIsAdmin(enhance(ExplorePage))
