import React from 'react'
import PropTypes from 'prop-types'
import CardMedia from '@material-ui/core/CardMedia'

const File = ({ url, mediaType, classes }) => (
  <CardMedia
    className={classes}
    component={mediaType}
    src={url}
    controls
    draggable={false}
  />
)

File.propTypes = {
  url: PropTypes.string.isRequired,
  mediaType: PropTypes.string.isRequired,
  classes: PropTypes.object
}

export default File
