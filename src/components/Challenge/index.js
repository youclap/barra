import Challenge from './Challenge'
import enhance from './Challenge.enhancer'

export default enhance(Challenge)
