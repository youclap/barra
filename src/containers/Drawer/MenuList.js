import React from 'react'
import ReactMenuList from '@material-ui/core/MenuList'
import MenuItem from '@material-ui/core/MenuItem'
import ListItemIcon from '@material-ui/core/ListItemIcon'
import ViewModuleIcon from '@material-ui/icons/ViewModule'
import ReportProblemIcon from '@material-ui/icons/ReportProblem'
import VideoCamIcon from '@material-ui/icons/Videocam'
import ExploreIcon from '@material-ui/icons/Explore'
import { HOME_PATH, REPORT_PATH, EXPLORE_PATH, CHALLENGE_PATH } from 'constants'
import { Link } from 'react-router'
import classes from './MenuList.scss'

const MenuList = () => (
  <ReactMenuList className={classes.menu}>
    <MenuItem className={classes.menuItem} component={Link} to={HOME_PATH}>
      <ListItemIcon className={classes.icon}>
        <ViewModuleIcon />
      </ListItemIcon>
      Home
    </MenuItem>
    <MenuItem className={classes.menuItem} component={Link} to={REPORT_PATH}>
      <ListItemIcon className={classes.icon}>
        <ReportProblemIcon />
      </ListItemIcon>
      Report
    </MenuItem>
    <MenuItem className={classes.menuItem} component={Link} to={EXPLORE_PATH}>
      <ListItemIcon className={classes.icon}>
        <ExploreIcon />
      </ListItemIcon>
      Explore
    </MenuItem>
    <MenuItem className={classes.menuItem} component={Link} to={CHALLENGE_PATH}>
      <ListItemIcon className={classes.icon}>
        <VideoCamIcon />
      </ListItemIcon>
      Challenges
    </MenuItem>
  </ReactMenuList>
)

export default MenuList
