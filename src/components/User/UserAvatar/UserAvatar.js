import React from 'react'
import PropTypes from 'prop-types'
import FileStorage from 'modules/fileStorage'
import Avatar from '@material-ui/core/Avatar'

const UserAvatar = ({ user, styles }) => (
    <div>
        {user.photoPath ?
            (
                <Avatar
                    className={styles.avatarSize}>
                    <FileStorage
                        path={user.photoPath}
                        mediaType={'img'}
                        id={user.id}
                    />
                </Avatar >
            ) :
            (
                <Avatar
                    className={styles.avatarSize}
                    alt={user.username}
                    src={user.photoURL} />
            )}
    </div>
)

UserAvatar.propTypes = {
    user: PropTypes.object.isRequired,
    styles: PropTypes.object.isRequired
}

export default UserAvatar
