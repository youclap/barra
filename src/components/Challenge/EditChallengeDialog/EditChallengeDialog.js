import React from 'react'
import PropTypes from 'prop-types'
import Button from '@material-ui/core/Button'
import IconButton from '@material-ui/core/IconButton'
import Tooltip from '@material-ui/core/Tooltip'
import Dialog from '@material-ui/core/Dialog'
import DialogActions from '@material-ui/core/DialogActions'
import DialogContent from '@material-ui/core/DialogContent'
import DialogContentText from '@material-ui/core/DialogContentText'
import DialogTitle from '@material-ui/core/DialogTitle'
import Edit from '@material-ui/icons/Edit'
import { required, isEndDateValid } from 'utils/form'
import { Field, reduxForm, Form } from 'redux-form'
import { TextField } from 'redux-form-material-ui'
import classes from './EditChallengeDialog.scss'

export default class EditChallengeDialog extends React.Component {
  state = {
    open: false
  }

  handleClickOpen = () => {
    this.setState({ open: true })
  }

  handleClose = () => {
    this.setState({ open: false })
  }


  render() {
    const { handleSubmit, pristine, reset, submitting } = this.props
    return (
      <div className={classes.container}>
        <IconButton className={classes.color} onClick={this.handleClickOpen}>
        <Tooltip title="Edit Challenge" placement="bottom">
          <Edit />
          </Tooltip>
        </IconButton>
        <Dialog
          open={this.state.open}
          onClose={this.handleClose}
          aria-labelledby="form-dialog-title"
        >
          <DialogTitle id="form-dialog-title">Challenge's information</DialogTitle>
          <DialogContent>
            <DialogContentText>
              Now you can change challenge's information without annoying your friends 👌
              <Form>
                <Field
                  name="challengeTitle"
                  component={TextField}
                  validate={[required]}
                  label="Challenge Title"
                  className={classes.textField}
                  fullWidth
                  margin="normal"
                  variant="filled"
                  InputLabelProps={{
                    shrink: true
                  }}
                />
                <Field
                  name="messageDescription"
                  component={TextField}
                  validate={[required]}
                  id="filled-name"
                  label="Description"
                  className={classes.textField}
                  fullWidth
                  multiline
                  rows="3"
                  margin="normal"
                  variant="filled"
                  InputLabelProps={{
                    shrink: true
                  }}
                />
                <Field
                  name="startDate"
                  component={TextField}
                  validate={[required]}
                  id="date"
                  label="Start Date"
                  type="datetime-local"
                  InputLabelProps={{
                    shrink: true
                  }}
                />
                <Field
                  name="endDate"
                  component={TextField}
                  validate={[isEndDateValid]}
                  id="date"
                  label="End Date"
                  type="datetime-local"
                  InputLabelProps={{
                    shrink: true
                  }}
                />
              </Form>
            </DialogContentText>
          </DialogContent>
          <DialogActions>
            <Button onClick={this.handleClose}color="primary">
              Cancel
            </Button>
            <Button onClick={() => this.submitChallenge()} color="primary">
              Change
            </Button>
          </DialogActions>
        </Dialog>
      </div>
    )
  }
}
