import { isTextPost, createPostTextObject } from 'utils/media'

export class UserPagePostViewModel {
    constructor(post) {
        const isText = isTextPost(post.media.type)

        this.id = post.uid
        this.text = isText ? createPostTextObject(post) : undefined
        this.path = isText ? undefined : 'challenge/' + post.challengeID + '/post/' + post.uid + '/thumb_media'
        this.challengeID = post.challengeID
        this.isDeleted = post.deleted
    }
}