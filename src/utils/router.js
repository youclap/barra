import { connectedReduxRedirect, connectedRouterRedirect } from 'redux-auth-wrapper/history3/redirect'
import locationHelperBuilder from 'redux-auth-wrapper/history3/locationHelper'
import connectedAuthWrapper from 'redux-auth-wrapper/connectedAuthWrapper'
import { HOME_PATH, LOGIN_PATH } from 'constants'
import LoadingSpinner from 'components/LoadingSpinner'
import AccessDenied from 'components/AccessDenied'

const locationHelper = locationHelperBuilder({})

export const UserIsNotAuthenticated = connectedRouterRedirect({
  redirectPath: (state, ownProps) => locationHelper.getRedirectQueryParam(ownProps) || HOME_PATH,
  allowRedirectBack: false,
  authenticatedSelector: ({ firebase: { auth } }) => auth.isEmpty,
  wrapperDisplayName: 'UserIsNotAuthenticated'
})

export const UserIsAuthenticated = connectedReduxRedirect({
  redirectPath: LOGIN_PATH,
  authenticatedSelector: ({ firebase: { auth } }) => auth !== null,
  authenticatingSelector: ({ firebase: { auth } }) => !auth.isLoaded,
  AuthenticatingComponent: LoadingSpinner,
  wrapperDisplayName: 'UserIsAuthenticated'
})

export const UserIsAdmin = (Component) => connectedAuthWrapper({
  authenticatedSelector: ({ firebase: { auth, profile } }) => auth !== null && profile !== null && profile.role === 'admin',
  wrapperDisplayName: 'UserIsAdmin',
  FailureComponent: AccessDenied
})(Component)
