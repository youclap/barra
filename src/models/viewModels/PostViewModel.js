import moment from 'moment'
import { getHTMLElement, isTextPost, createPostTextObject } from 'utils/media'

const dateFormat = 'DD MM YYYY hh:mm a'

export class PostViewModel {
  constructor(post) {
    const isText = isTextPost(post.media.type)
    const htmlElement = getHTMLElement(post.media.type)

    this.id = post.uid
    this.text = isText ? createPostTextObject(post) : undefined
    this.mediaType = isText ? 'TEXT' : htmlElement
    this.path = isText
      ? undefined
      : 'challenge/' + post.challengeID + '/post/' + post.uid + '/media'
    this.date = moment.unix(post.timestamp.seconds).format(dateFormat)
    this.claps = post.claps
    this.isDeleted = post.deleted
    this.challengeID = post.challengeID
    this.userID = post.userID
  }
}
