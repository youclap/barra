export const ReportType = {
  CHALLENGE: 'challenge',
  PROFILE: 'profile',
  POST: 'post',
  COMMENT: 'comment'
}
