import FeaturedUser from './FeaturedUser'
import enhance from 'components/User/User.enhancer'
import featuredEnhance from './FeaturedUser.enhancer'

export default enhance(featuredEnhance(FeaturedUser))
