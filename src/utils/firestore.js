export const getDocument = (firestore, collectionName, docID) => {
    return firestore
        .collection(collectionName)
        .doc(docID)
        .get()
        .then(documentSnapshot => {
            if (documentSnapshot.exists) {
                return documentSnapshot.data()
            } else {
                console.error('document collection ', collectionName, '/', docID, ' doesn\'t exist!')
                return null
            }
        })
}

export const documentIsAvailable = (document) => {
    return !document.deleted
}
