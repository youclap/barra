import React from 'react'
import PropTypes from 'prop-types'
import User from 'components/User'
import Challenge from 'components/Challenge'
import Post from 'components/Post'
import Comment from 'components/Comment'
import ConfirmationDeleteDialog from 'components/ConfirmationDeleteDialog'
import Button from '@material-ui/core/Button'
import Card from '@material-ui/core/Card'
import Grid from '@material-ui/core/Grid'
import ListItem from '@material-ui/core/ListItem'
import ListItemText from '@material-ui/core/ListItemText'
import Table from '@material-ui/core/Table'
import TableBody from '@material-ui/core/TableBody'
import TableCell from '@material-ui/core/TableCell'
import TableHead from '@material-ui/core/TableHead'
import TableRow from '@material-ui/core/TableRow'
import Report from '@material-ui/icons/Report'
import classes from './ReportItem.scss'

const ReportItem = ({
  item,
  reports,
  newDialogIgnoreOpen,
  newDialogDeleteOpen,
  toggleIgnoreDialog,
  toggleDeleteDialog,
  ignoreReportItem,
  deleteItem
}) => {
  return (
    <Card className={classes.card}>
      {renderReportComponent(item)}
      <ListItem>
        <div className={classes.listItem}>
          <Report className={classes.icon} />
          <ListItemText
            className={classes.itemText}
            primary={reports.length}
            secondary="Number of reports to this item"
          />
        </div>
      </ListItem>
      <Table>
        <TableHead>
          <TableRow>
            <TableCell>report id</TableCell>
            <TableCell>status</TableCell>
            <TableCell>value</TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {reports.map(report => (
            <TableRow key={report.reportID}>
              <TableCell>{report.reportID}</TableCell>
              <TableCell>{report.status}</TableCell>
              <TableCell>{report.value}</TableCell>
            </TableRow>
          ))}
        </TableBody>
      </Table>
      <Grid
        className={classes.grid}
        container
        direction="row"
        justify="space-evenly"
        alignItems="center"
      >
        <h4>
          You were chosen to decide the fate of this {item.type.toLowerCase()}!
          👩‍⚖️👨‍⚖️
        </h4>
        <ConfirmationDeleteDialog
          dialogText={`By clicking 'OK' you will mark these reports as ignored.
          The upcoming reports will also be marked as ignored and the item will not be deleted.
          If you are not sure please fall back and think again.`}
          open={newDialogIgnoreOpen}
          onRequestClose={() => toggleIgnoreDialog()}
          onSubmit={() => ignoreReportItem(item)} />
        <Button
          variant="contained"
          color="primary"
          className={classes.button}
          onClick={toggleIgnoreDialog}
        >
          Ignore
        </Button>

        <ConfirmationDeleteDialog
          dialogText={`By clicking 'OK' you will mark this item as deleted!
          These reports and the upcoming will also be marked as resolved.
          The item creator will be notified by email that his content was removed from YouClap because it goes against
          your terms and conditions. If you are not sure please fall back and think again.`}
          open={newDialogDeleteOpen}
          onRequestClose={() => toggleDeleteDialog()}
          onSubmit={() => deleteItem(item)} />
        <Button
          variant="contained"
          color="secondary"
          className={classes.button}
          onClick={toggleDeleteDialog}
        >
          Delete
        </Button>
      </Grid>
    </Card>
  )
}

const renderReportComponent = (item) => {
  const itemType = item.type
  const itemID = item.id
  if (itemType === 'CHALLENGE') {
    return <Challenge id={itemID} />
  } else if (itemType === 'POST') {
    return <Post id={itemID} />
  } else if (itemType === 'USER') {
    return <User id={itemID} />
  } else if (itemType === 'COMMENT') {
    return <Comment id={itemID} />
  }
}

ReportItem.propTypes = {
  item: PropTypes.object.isRequired,
  reports: PropTypes.array,
  ignoreReportItem: PropTypes.func,
  deleteItem: PropTypes.func,
  newDialogIgnoreOpen: PropTypes.bool,
  newDialogDeleteOpen: PropTypes.bool,
  toggleIgnoreDialog: PropTypes.func,
  toggleDeleteDialog: PropTypes.func
}

export default ReportItem
