import EditChallengeDialog from './EditChallengeDialog'
import enhance from './EditChallengeDialog.enhancer'

export default enhance(EditChallengeDialog)
