import { compose } from 'redux'
import { connect } from 'react-redux'
import { firestoreConnect } from 'react-redux-firebase'
import { withHandlers, withStateHandlers, pure } from 'recompose'
import { spinnerWhileLoading, withRouter } from 'utils/components'
import { withNotifications } from 'modules/notification'
import { ChallengeViewModel } from 'models/viewModels/ChallengeViewModel'
import { updateDocumentField } from 'utils/updateField'
import { addDocumentToExplore, featuredChallengesCount } from 'utils/explore'
import { CHALLENGE_PATH } from 'constants'

export default compose(
  firestoreConnect(({ id }) => [
    {
      collection: 'challenge',
      doc: id
    }
  ]),
  connect(({ firestore: { data } }, { id, showAddToExploreButton }) => {
    if (!data.challenge || !data.challenge[id] || !data.challenge[id].userID) {
      return {}
    }
    const challenge = data.challenge[id]
    const challengeViewModel = new ChallengeViewModel(challenge)
    return { challenge: challengeViewModel, showAddToExploreButton }
  }),
  spinnerWhileLoading(['challenge']),
  withRouter,
  withNotifications,
  withStateHandlers(
    ({ initialDialogOpen = false }) => ({
      newDialogOpen: initialDialogOpen,
      addToExploreDialogOpen: initialDialogOpen
    }),
    {
      toggleDialog: ({ newDialogOpen }) => () => ({
        newDialogOpen: !newDialogOpen
      }),

      toggleAddToExploreDialog: ({ addToExploreDialogOpen }) => () => ({
        addToExploreDialogOpen: !addToExploreDialogOpen
      })
    }
  ),
  withHandlers({
    markChallengeAsDeleted: ({ firestore, challenge, showSuccess, showError }) => () => {
      return updateDocumentField(firestore, 'challenge', challenge, 'deleted', true)
        .then(() => showSuccess('Challenge marked as deleted!'))
        .catch(error => {
          console.error(error.message)
          return showError('Failed marking challenge as deleted!')
        })
    },
    restoreChallenge: ({ firestore, challenge, showSuccess, showError }) => () => {
      return updateDocumentField(firestore, 'challenge', challenge, 'deleted', false)
        .then(() => showSuccess('Challenge restored with success!'))
        .catch(error => {
          console.error(error.message)
          return showError('Failed restoring deleted challenge!')
        })
    },
    goToChallenge: ({ challenge, router }) => () => {
      router.push({
        pathname: `${CHALLENGE_PATH}/${challenge.id}`,
        state: challenge
      })
    },
    addToExplore: ({ firestore, challenge, toggleAddToExploreDialog, showSuccess, showError }) => featuredChallenge => {
      toggleAddToExploreDialog()
      const order = parseInt(featuredChallenge.order)
      if (order) {
        return addDocumentToExplore(firestore, challenge.id, 'CHALLENGE', order)
          .then(() => showSuccess('Featured Card created with success!'))
          .catch(error => {
            console.error('Error:', error)
            return showError(error.message || 'Error creating explore card ' + challenge.id)
          })
      } else {
        return featuredChallengesCount(firestore)
          .then(challengesCount => {
            return addDocumentToExplore(firestore, challenge.id, 'CHALLENGE', challengesCount)
              .then(() => showSuccess('Featured Card created with success!'))
              .catch(error => {
                console.error('Error:', error)
                return showError(error.message || 'Error creating explore card ' + challenge.id)
              })
          })
      }
    }
  }),
  pure
)
