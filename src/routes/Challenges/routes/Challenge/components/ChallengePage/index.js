import ChallengePage from './ChallengePage'
import enhance from './ChallengePage.enhancer'
import { UserIsAdmin } from 'utils/router'

export default UserIsAdmin(enhance(ChallengePage))
