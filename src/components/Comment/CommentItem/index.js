import CommentItem from './CommentItem'
import enhance from '../Comment.enhancer'
import enhanceItem from './CommentItem.enhancer'

export default enhance(enhanceItem(CommentItem))
