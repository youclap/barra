import Post from './Post'
import enhance from './Post.enhancer'

export default enhance(Post)
