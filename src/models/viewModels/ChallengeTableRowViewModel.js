import moment from 'moment'
import { challengeIsActive } from 'utils/challenge'
const dateFormat = 'DD/MM/YYYY'

export class ChallengeTableRowViewModel {
  constructor(challenge) {
    const startDateMoment = moment.unix(challenge.startDate.seconds)
    const endDateMoment = moment.unix(challenge.endDate.seconds)

    this.id = challenge.uid
    this.title = challenge.title
    this.userID = challenge.userID
    this.status = (isActive(startDateMoment, endDateMoment) && !challenge.deleted) ? '✅' : '❌'
    this.startDateMoment = startDateMoment
    this.endDateMoment = endDateMoment
    this.startDate = startDateMoment.format(dateFormat)
    this.endDate = endDateMoment.format(dateFormat)
    this.thumbnail = 'challenge/' + challenge.uid + '/thumb_media'
  }
}

const isActive = (startDateMoment, endDateMoment) => {
  const momentNow = moment()
  return momentNow.diff(startDateMoment) >= 0 &&
    endDateMoment.diff(momentNow) >= 0
}
