import { reduxForm } from 'redux-form'
import { NEW_FEATURED_CARD_WITH_ORDER } from 'constants'

export default reduxForm({
    form: NEW_FEATURED_CARD_WITH_ORDER,
    onSubmitSuccess: (result, dispatch, props) => props.reset()
})
