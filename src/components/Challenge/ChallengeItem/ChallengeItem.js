import React from 'react'
import PropTypes from 'prop-types'
import FileStorage from 'modules/fileStorage'
import { showDelete } from '../Challenge'
import Card from '@material-ui/core/Card'
import CardContent from '@material-ui/core/CardContent'
import Typography from '@material-ui/core/Typography'
import IconButton from '@material-ui/core/IconButton'
import classes from './ChallengeItem.scss'

const ChallengeItem = ({
  challenge,
  goToChallenge,
  markChallengeAsDeleted,
  restoreChallenge,
  newDialogOpen,
  toggleDialog
}) => (
  <div className={classes.item}>
    <Card className={classes.card}>
      <FileStorage
        className={classes.media}
        path={challenge.path}
        mediaType={'img'}
        id={challenge.id}
      />
      <CardContent>
        <IconButton className={classes.icon} onClick={toggleDialog}>
          {showDelete(
            challenge,
            newDialogOpen,
            toggleDialog,
            markChallengeAsDeleted,
            restoreChallenge
          )}
        </IconButton>
        <Typography
          gutterBottom
          variant="headline"
          component="h2"
          className={classes.title}
          onClick={() => goToChallenge(challenge)}
        >
          {challenge.title}
        </Typography>
      </CardContent>
    </Card>
  </div>
)

ChallengeItem.propTypes = {
  challenge: PropTypes.object.isRequired,
  goToChallenge: PropTypes.func.isRequired,
  newDialogOpen: PropTypes.func.isRequired,
  toggleDialog: PropTypes.func.isRequired,
  markChallengeAsDeleted: PropTypes.isRequired,
  restoreChallenge: PropTypes.isRequired
}

export default ChallengeItem
