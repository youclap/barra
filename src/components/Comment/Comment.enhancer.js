import { compose } from 'redux'
import { connect } from 'react-redux'
import { firestoreConnect } from 'react-redux-firebase'
import { withHandlers, withStateHandlers, pure } from 'recompose'
import { spinnerWhileLoading } from 'utils/components'
import { withNotifications } from 'modules/notification'
import { CommentViewModel } from 'models/viewModels/CommentViewModel'
import { updateDocumentField } from 'utils/updateField'

export default compose(
  firestoreConnect(({ id }) => [
    {
      collection: 'comment',
      doc: id
    }
  ]),
  connect(({ firestore: { data } }, { id }) => {
    if (!data.comment || !data.comment[id]) {
      return {}
    }
    const comment = data.comment[id]
    const commentViewModel = new CommentViewModel(comment)
    return { comment: commentViewModel }
  }),
  spinnerWhileLoading(['comment']),
  withNotifications,
  withStateHandlers(
    ({ initialDialogOpen = false }) => ({
      newDialogOpen: initialDialogOpen
    }),
    {
      toggleDialog: ({ newDialogOpen }) => () => ({
        newDialogOpen: !newDialogOpen
      })
    }
  ),
  withHandlers({
    markAsDeleted: ({ firestore, comment, showSuccess, showError }) => () => {
      return updateDocumentField(firestore, 'comment', comment, 'deleted', true)
        .then(() => showSuccess('Comment marked as deleted!'))
        .catch(error => {
          console.error(error.message)
          return showError('Failed marking comment as deleted!')
        })
    },

    restoreComment: ({ firestore, comment, showSuccess, showError }) => () => {
      return updateDocumentField(firestore, 'comment', comment, 'deleted', false)
        .then(() => showSuccess('Comment restored with success!'))
        .catch(error => {
          console.error(error.message)
          return showError('Failed restoring deleted comment!')
        })
    },

    goToComment: props => comment => {
      console.log('go to comment ', comment, ' with props ', props)
    }
  }),
  pure
)
