import FeaturedChallenge from './FeaturedChallenge'
import enhance from 'components/Challenge/Challenge.enhancer'
import featuredEnhance from './FeaturedChallenge.enhancer'

export default enhance(featuredEnhance(FeaturedChallenge))
