import { connect } from 'react-redux'
import { compose } from 'redux'
import { spinnerWhileLoading } from 'utils/components'
import { UserPageChallengeViewModel } from 'models/viewModels/UserPageChallengeViewModel'

export default compose(
    connect(({ firestore: { data } }, { id }) => {
        if (!data || !data.challenge || !id) {
            return {}
        }
        const userChallengeViewModel = new UserPageChallengeViewModel(data.challenge[id])
        return { challenge: userChallengeViewModel }
    }),
    spinnerWhileLoading(['challenge'])
)
